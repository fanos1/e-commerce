
-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_session_id` char(32) NOT NULL,
  `product_type` enum('goodies') NOT NULL,
  `product_id` mediumint(8) unsigned NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL,
  `size` varchar(60) DEFAULT NULL,
  `image` varchar(70) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `product_type` (`product_type`,`product_id`),
  KEY `user_session_id` (`user_session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=458 ;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_session_id`, `product_type`, `product_id`, `quantity`, `size`, `image`, `date_created`, `date_modified`) VALUES
(1, '90489d88733ad549c06bc33c897fc973', 'goodies', 1, 4, 'S/M', NULL, '2015-12-06 17:02:30', '2015-12-06 17:14:34'),
(3, 'b91d5eb68c9e4a2c6ed1d1c3c06f0bff', 'goodies', 1, 2, NULL, NULL, '2015-12-07 18:13:46', '2015-12-07 18:21:07'),
(4, 'b91d5eb68c9e4a2c6ed1d1c3c06f0bff', 'goodies', 2, 1, NULL, NULL, '2015-12-07 18:14:24', '0000-00-00 00:00:00'),
(6, '8a321e753f59af73433e0f0e230c25bc', 'goodies', 1, 2, NULL, NULL, '2015-12-10 18:59:22', '2015-12-10 19:49:03'),
(450, 'f2ebb89f1a1620bf907273d090384710', 'goodies', 6, 1, 'M/L', NULL, '2016-03-04 06:07:57', '2016-03-04 06:08:05')
;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(40) NOT NULL,
  `description` tinytext NOT NULL,
  `image` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `description`, `image`) VALUES
(1, 'Jumpers', 'best clothes and more!', 'clothes.jpg'),
(2, 'tunics', 'High quality printed knitted tunic dresses for the discerning ladies.', ''),
(3, 'Cardigans', 'best cardigas', '781426_32573620.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `charges`
--

CREATE TABLE IF NOT EXISTS `charges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `charge_id` varchar(60) NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `type` varchar(18) NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `charge` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `charge_id` (`charge_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `charges`
--

INSERT INTO `charges` (`id`, `charge_id`, `order_id`, `type`, `amount`, `charge`, `date_created`) VALUES
(19, 'xxx', 25, 'auth_only', 2600, 'O:13:\\"Stripe\\\\Charge\\":6:{s:8:\\"\\0*\\0_opts\\";O:26:\\"Stripe\\\\Util\\\\RequestOptions\\":2:{s:7:\\"headers\\";a:0:{}s:6:\\"apiKey\\";s:32:\\"sk_test_eYJO5FcU2jtUaPS1ozXMqmD9\\";}s:10:\\"\\0*\\0_values\\";a:31:{s:2:\\"id\\";s:17:\\"ch_7liy91iftRZ50L\\";s:6:\\"object\\";s:6:\\"charge\\";s:6:\\"amount\\";i:2600;s:15:\\"amount_refunded\\";i:0;s:15:\\"application_fee\\";N;s:19:\\"balance_transaction\\";N;s:8:\\"captured\\";b:0;s:4:\\"card\\";O:11:\\"Stripe\\\\Card\\":6:{s:8:\\"\\0*\\0_opts\\";r:2;s:10:\\"\\0*\\0_values\\";a:24:{s:2:\\"id\\";s:19:\\"card_7liy0nABtKvN7c\\";s:6:\\"object\\";s:4:\\"card\\";s:12:\\"address_city\\";N;s:15:\\"address_country\\";N;s:13:\\"address_line1\\";N;s:19:\\"address_line1_check\\";N;s:13:\\"address_line2\\";N;s:13:\\"address_state\\";N;s:11:\\"address_zip\\";N;s:17:\\"address_zip_check\\";N;s:5:\\"brand\\";s:4:\\"Visa\\";s:7:\\"country\\";s:2:\\"US\\";s:8:\\"customer\\";N;s:9:\\"cvc_check\\";s:4:\\"pass\\";s:13:\\"dynamic_last4\\";N;s:9:\\"exp_month\\";i:6;s:8:\\"exp_year\\";i:2019;s:11:\\"fingerprint\\";s:16:\\"9IrZs7VwKo8In14G\\";s:7:\\"funding\\";s:5:\\"debit\\";s:5:\\"last4\\";s:4:\\"5556\\";s:8:\\"metadata\\";O:21:\\"Stripe\\\\AttachedObject\\":6:{s:8:\\"\\0*\\0_opts\\";r:2;s:10:\\"\\0*\\0_values\\";a:0:{}s:17:\\"\\0*\\0_unsavedValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_transientValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_retrieveOptions\\";a:0:{}s:16:\\"\\0*\\0_lastResponse\\";N;}s:4:\\"name\\";s:18:\\"firstname lastname\\";s:19:\\"tokenization_method\\";N;s:4:\\"type\\";s:4:\\"Visa\\";}s:17:\\"\\0*\\0_unsavedValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_transientValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_retrieveOptions\\";a:0:{}s:16:\\"\\0*\\0_lastResponse\\";N;}s:7:\\"created\\";i:1453494501;s:8:\\"currency\\";s:3:\\"gbp\\";s:8:\\"customer\\";N;s:11:\\"description\\";s:25:\\"fanisxanthiotis@gmail.com\\";s:11:\\"destination\\";N;s:7:\\"dispute\\";N;s:12:\\"failure_code\\";N;s:15:\\"failure_message\\";N;s:13:\\"fraud_details\\";a:0:{}s:7:\\"invoice\\";N;s:8:\\"livemode\\";b:0;s:8:\\"metadata\\";O:21:\\"Stripe\\\\AttachedObject\\":6:{s:8:\\"\\0*\\0_opts\\";r:2;s:10:\\"\\0*\\0_values\\";a:0:{}s:17:\\"\\0*\\0_unsavedValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_transientValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_retrieveOptions\\";a:0:{}s:16:\\"\\0*\\0_lastResponse\\";N;}s:5:\\"order\\";N;s:4:\\"paid\\";b:1;s:13:\\"receipt_email\\";N;s:14:\\"receipt_number\\";N;s:8:\\"refunded\\";b:0;s:7:\\"refunds\\";a:0:{}s:8:\\"shipping\\";N;s:6:\\"source\\";O:11:\\"Stripe\\\\Card\\":6:{s:8:\\"\\0*\\0_opts\\";r:2;s:10:\\"\\0*\\0_values\\";a:24:{s:2:\\"id\\";s:19:\\"card_7liy0nABtKvN7c\\";s:6:\\"object\\";s:4:\\"card\\";s:12:\\"address_city\\";N;s:15:\\"address_country\\";N;s:13:\\"address_line1\\";N;s:19:\\"address_line1_check\\";N;s:13:\\"address_line2\\";N;s:13:\\"address_state\\";N;s:11:\\"address_zip\\";N;s:17:\\"address_zip_check\\";N;s:5:\\"brand\\";s:4:\\"Visa\\";s:7:\\"country\\";s:2:\\"US\\";s:8:\\"customer\\";N;s:9:\\"cvc_check\\";s:4:\\"pass\\";s:13:\\"dynamic_last4\\";N;s:9:\\"exp_month\\";i:6;s:8:\\"exp_year\\";i:2019;s:11:\\"fingerprint\\";s:16:\\"9IrZs7VwKo8In14G\\";s:7:\\"funding\\";s:5:\\"debit\\";s:5:\\"last4\\";s:4:\\"5556\\";s:8:\\"metadata\\";O:21:\\"Stripe\\\\AttachedObject\\":6:{s:8:\\"\\0*\\0_opts\\";r:2;s:10:\\"\\0*\\0_values\\";a:0:{}s:17:\\"\\0*\\0_unsavedValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_transientValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_retrieveOptions\\";a:0:{}s:16:\\"\\0*\\0_lastResponse\\";N;}s:4:\\"name\\";s:18:\\"firstname lastname\\";s:19:\\"tokenization_method\\";N;s:4:\\"type\\";s:4:\\"Visa\\";}s:17:\\"\\0*\\0_unsavedValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_transientValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_retrieveOptions\\";a:0:{}s:16:\\"\\0*\\0_lastResponse\\";N;}s:20:\\"statement_descriptor\\";N;s:6:\\"status\\";s:4:\\"paid\\";s:21:\\"statement_description\\";N;}s:17:\\"\\0*\\0_unsavedValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_transientValues\\";O:15:\\"Stripe\\\\Util\\\\Set\\":1:{s:22:\\"\\0Stripe\\\\Util\\\\Set\\0_elts\\";a:0:{}}s:19:\\"\\0*\\0_retrieveOptions\\";a:0:{}s:16:\\"\\0*\\0_lastResponse\\";O:18:\\"Stripe\\\\ApiResponse\\":4:{s:7:\\"headers\\";a:13:{s:6:\\"Server\\";s:5:\\"nginx\\";s:4:\\"Date\\";s:29:\\"Fri, 22 Jan 2016 20:28:21 GMT\\";s:12:\\"Content-Type\\";s:16:\\"application/json\\";s:14:\\"Content-Length\\";s:4:\\"1980\\";s:10:\\"Connection\\";s:10:\\"keep-alive\\";s:32:\\"Access-Control-Allow-Credentials\\";s:4:\\"true\\";s:28:\\"Access-Control-Allow-Methods\\";s:32:\\"GET, POST, HEAD, OPTIONS, DELETE\\";s:27:\\"Access-Control-Allow-Origin\\";s:1:\\"*\\";s:22:\\"Access-Control-Max-Age\\";s:3:\\"300\\";s:13:\\"Cache-Control\\";s:18:\\"no-cache, no-store\\";s:10:\\"Request-Id\\";s:18:\\"req_7liyHDBpRvn1DZ\\";s:14:\\"Stripe-Version\\";s:10:\\"2014-01-31\\";s:25:\\"Strict-Transport-Security\\";s:35:\\"max-age=31556926; includeSubDomains\\";}s:4:\\"body\\";s:1980:\\"{\n  \\"id\\": \\"ch_7liy91iftRZ50L\\",\n  \\"object\\": \\"charge\\",\n  \\"amount\\": 2600,\n  \\"amount_refunded\\": 0,\n  \\"application_fee\\": null,\n  \\"balance_transaction\\": null,\n  \\"captured\\": false,\n  \\"card\\": {\n    \\"id\\": \\"card_7liy0nABtKvN7c\\",\n    \\"object\\": \\"card\\",\n    \\"address_city\\": null,\n    \\"address_country\\": null,\n    \\"address_line1\\": null,\n    \\"address_line1_check\\": null,\n    \\"address_line2\\": null,\n    \\"address_state\\": null,\n    \\"address_zip\\": null,\n    \\"address_zip_check\\": null,\n    \\"brand\\": \\"Visa\\",\n    \\"country\\": \\"US\\",\n    \\"customer\\": null,\n    \\"cvc_check\\": \\"pass\\",\n    \\"dynamic_last4\\": null,\n    \\"exp_month\\": 6,\n    \\"exp_year\\": 2019,\n    \\"fingerprint\\": \\"9IrZs7VwKo8In14G\\",\n    \\"funding\\": \\"debit\\",\n    \\"last4\\": \\"5556\\",\n    \\"metadata\\": {},\n    \\"name\\": \\"firstname lastname\\",\n    \\"tokenization_method\\": null,\n    \\"type\\": \\"Visa\\"\n  },\n  \\"created\\": 1453494501,\n  \\"currency\\": \\"gbp\\",\n  \\"customer\\": null,\n  \\"description\\": \\"fanisxanthiotis@gmail.com\\",\n  \\"destination\\": null,\n  \\"dispute\\": null,\n  \\"failure_code\\": null,\n  \\"failure_message\\": null,\n  \\"fraud_details\\": {},\n  \\"invoice\\": null,\n  \\"livemode\\": false,\n  \\"metadata\\": {},\n  \\"order\\": null,\n  \\"paid\\": true,\n  \\"receipt_email\\": null,\n  \\"receipt_number\\": null,\n  \\"refunded\\": false,\n  \\"refunds\\": [],\n  \\"shipping\\": null,\n  \\"source\\": {\n    \\"id\\": \\"card_7liy0nABtKvN7c\\",\n    \\"object\\": \\"card\\",\n    \\"address_city\\": null,\n    \\"address_country\\": null,\n    \\"address_line1\\": null,\n    \\"address_line1_check\\": null,\n    \\"address_line2\\": null,\n    \\"address_state\\": null,\n    \\"address_zip\\": null,\n    \\"address_zip_check\\": null,\n    \\"brand\\": \\"Visa\\",\n    \\"country\\": \\"US\\",\n    \\"customer\\": null,\n    \\"cvc_check\\": \\"pass\\",\n    \\"dynamic_last4\\": null,\n    \\"exp_month\\": 6,\n    \\"exp_year\\": 2019,\n    \\"fingerprint\\": \\"9IrZs7VwKo8In14G\\",\n    \\"funding\\": \\"debit\\",\n    \\"last4\\": \\"5556\\",\n    \\"metadata\\": {},\n    \\"name\\": \\"firstname lastname\\",\n    \\"tokenization_method\\": null,\n    \\"type\\": \\"Visa\\"\n  },\n  \\"statement_descriptor\\": null,\n  \\"status\\": \\"paid\\",\n  \\"statement_description\\": null\n}\n\\";s:4:\\"json\\";a:31:{s:2:\\"id\\";s:17:\\"ch_7liy91iftRZ50L\\";s:6:\\"object\\";s:6:\\"charge\\";s:6:\\"amount\\";i:2600;s:15:\\"amount_refunded\\";i:0;s:15:\\"application_fee\\";N;s:19:\\"balance_transaction\\";N;s:8:\\"captured\\";b:0;s:4:\\"card\\";a:24:{s:2:\\"id\\";s:19:\\"card_7liy0nABtKvN7c\\";s:6:\\"object\\";s:4:\\"card\\";s:12:\\"address_city\\";N;s:15:\\"address_country\\";N;s:13:\\"address_line1\\";N;s:19:\\"address_line1_check\\";N;s:13:\\"address_line2\\";N;s:13:\\"address_state\\";N;s:11:\\"address_zip\\";N;s:17:\\"address_zip_check\\";N;s:5:\\"brand\\";s:4:\\"Visa\\";s:7:\\"country\\";s:2:\\"US\\";s:8:\\"customer\\";N;s:9:\\"cvc_check\\";s:4:\\"pass\\";s:13:\\"dynamic_last4\\";N;s:9:\\"exp_month\\";i:6;s:8:\\"exp_year\\";i:2019;s:11:\\"fingerprint\\";s:16:\\"9IrZs7VwKo8In14G\\";s:7:\\"funding\\";s:5:\\"debit\\";s:5:\\"last4\\";s:4:\\"5556\\";s:8:\\"metadata\\";a:0:{}s:4:\\"name\\";s:18:\\"firstname lastname\\";s:19:\\"tokenization_method\\";N;s:4:\\"type\\";s:4:\\"Visa\\";}s:7:\\"created\\";i:1453494501;s:8:\\"currency\\";s:3:\\"gbp\\";s:8:\\"customer\\";N;s:11:\\"description\\";s:25:\\"fanisxanthiotis@gmail.com\\";s:11:\\"destination\\";N;s:7:\\"dispute\\";N;s:12:\\"failure_code\\";N;s:15:\\"failure_message\\";N;s:13:\\"fraud_details\\";a:0:{}s:7:\\"invoice\\";N;s:8:\\"livemode\\";b:0;s:8:\\"metadata\\";a:0:{}s:5:\\"order\\";N;s:4:\\"paid\\";b:1;s:13:\\"receipt_email\\";N;s:14:\\"receipt_number\\";N;s:8:\\"refunded\\";b:0;s:7:\\"refunds\\";a:0:{}s:8:\\"shipping\\";N;s:6:\\"source\\";a:24:{s:2:\\"id\\";s:19:\\"card_7liy0nABtKvN7c\\";s:6:\\"object\\";s:4:\\"card\\";s:12:\\"address_city\\";N;s:15:\\"address_country\\";N;s:13:\\"address_line1\\";N;s:19:\\"address_line1_check\\";N;s:13:\\"address_line2\\";N;s:13:\\"address_state\\";N;s:11:\\"address_zip\\";N;s:17:\\"address_zip_check\\";N;s:5:\\"brand\\";s:4:\\"Visa\\";s:7:\\"country\\";s:2:\\"US\\";s:8:\\"customer\\";N;s:9:\\"cvc_check\\";s:4:\\"pass\\";s:13:\\"dynamic_last4\\";N;s:9:\\"exp_month\\";i:6;s:8:\\"exp_year\\";i:2019;s:11:\\"fingerprint\\";s:16:\\"9IrZs7VwKo8In14G\\";s:7:\\"funding\\";s:5:\\"debit\\";s:5:\\"last4\\";s:4:\\"5556\\";s:8:\\"metadata\\";a:0:{}s:4:\\"name\\";s:18:\\"firstname lastname\\";s:19:\\"tokenization_method\\";N;s:4:\\"type\\";s:4:\\"Visa\\";}s:20:\\"statement_descriptor\\";N;s:6:\\"status\\";s:4:\\"paid\\";s:21:\\"statement_description\\";N;}s:4:\\"code\\";i:200;}}', '2016-01-22 20:29:20'),
;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(80) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `address1` varchar(80) NOT NULL,
  `address2` varchar(80) DEFAULT NULL,
  `city` varchar(60) NOT NULL,
  `state` char(2) NOT NULL,
  `zip` varchar(8) NOT NULL,
  `phone` char(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `email`, `first_name`, `last_name`, `address1`, `address2`, `city`, `state`, `zip`, `phone`, `date_created`) VALUES
(44, 'xxxx@outlook.com', 'xxxx', 'kstest', 'xxxxx road', NULL, 'london', 'AL', 'n16 xxx', '0755555555', '2016-01-23 18:50:34'),
(45, 'xxxx@outlook.com', 'xxxx', 'testlast', 'xxxxxx road', NULL, 'london', 'AL', 'n16 xxx', '555555555', '2016-01-23 19:00:10'),
;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `total` int(10) unsigned DEFAULT NULL,
  `shipping` int(10) unsigned NOT NULL DEFAULT '0',
  `credit_card_number` mediumint(4) unsigned zerofill NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_date` (`order_date`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `total`, `shipping`, `credit_card_number`, `order_date`) VALUES
(28, 4, 2500, 0, 1234, '2016-01-23 18:50:56'),
(29, 54, 500, 0, 1234, '2016-01-23 20:09:32'),
(30, 57, 1900, 0, 1234, '2016-01-24 13:47:51'),
(31, 58, 1800, 0, 1234, '2016-01-24 14:41:57'),
(32, 59, 1900, 0, 1234, '2016-02-14 16:12:18'),
(33, 61, 100, 0, 1234, '2016-02-21 16:01:24'),
(34, 62, 100, 0, 1234, '2016-02-27 15:08:29'),
(35, 63, 100, 0, 1234, '2016-03-05 12:05:48'),
(36, 64, 100, 0, 1234, '2016-03-05 15:14:01'),
(37, 66, 100, 0, 1234, '2016-03-06 12:54:30'),
(38, 67, 100, 0, 1234, '2016-03-06 12:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `order_contents`
--

CREATE TABLE IF NOT EXISTS `order_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `product_type` enum('coffee','goodies') DEFAULT NULL,
  `product_id` mediumint(8) unsigned NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL,
  `price_per` int(10) unsigned NOT NULL,
  `ship_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ship_date` (`ship_date`),
  KEY `product_type` (`product_type`,`product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `order_contents`
--

INSERT INTO `order_contents` (`id`, `order_id`, `product_type`, `product_id`, `quantity`, `price_per`, `ship_date`) VALUES
(19, 28, 'goodies', 17, 1, 2500, NULL),
(20, 29, 'goodies', 1, 1, 500, NULL),
(21, 30, 'goodies', 3, 1, 1900, NULL),
(22, 31, 'goodies', 17, 1, 1800, NULL),
(23, 32, 'goodies', 6, 1, 1900, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `non_coffee_category_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` text,
  `image` varchar(45) NOT NULL,
  `price` int(10) unsigned NOT NULL,
  `stock` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `size` int(11) DEFAULT NULL,
  `product_code` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `non_coffee_category_id` (`non_coffee_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `non_coffee_category_id`, `name`, `description`, `image`, `price`, `stock`, `size`, `product_code`, `date_created`) VALUES
(1, 3, 'Little Butterflies', 'This cardigan is medium weight and one size only designed to fit a UK size 8 - 14. Composition: 30% Nylon, 70% Viscose Cool hands wash 30 degree', 'd9996aee5639209b3fb618b07e10a34b27baad12.png', 1800, 100, 3, 'A2759 Beige', '2015-12-06 18:52:39'),
(2, 3, 'Shopping Girls', 'Soft stretch cotton and polyester fabric giving elegance looking perfect for over many garments and all many occasions. This cardigan is medium weight and one size only designed to fit a UK size 8 - 14. Composition: 65%cotton 35%polyester Cool hands wash 30 degree', 'cardigan-2970-beige.png', 1850, 4, 3, 'A2970 BEIGE', '2015-12-06 18:52:39'),
(3, 1, 'Mini Giraffes (Beige)', 'Loose fitting style, long sleeves. 80% Cotton, 20% Polyester.', '2725TBeige-600x860.png', 1700, 10, 1, '2725T Beige', '2015-12-14 18:11:44'),
(4, 3, 'A girl with an umbrella', 'Soft stretch cotton and polyester fabric giving elegance looking perfect for over many garments and all many occasions. Composition: 65%cotton 35%polyester Cool hands wash 30 degree', 'cardigan-2806-umbrella-girl.png', 1900, 0, 3, 'A2806 beige', '2015-12-26 16:02:57'),
(5, 3, 'A Girl With Bunnies', '-65% Cotton, 35% Polyester\r\n-Single jersey & stretchable', 'cardigan-3933-bunny-girl-beige.png', 2000, 0, 2, 'A3933 Beige', '2015-12-26 16:11:17'),
(6, 3, 'Rainbow Butterflies Beige', '-65% Cotton, 35% Polyester\r\n-Single jersey & stretchable', 'cardigan-3556-Rainbow-Butterflies-beige.png', 1900, 4, 3, 'A3556 Beige', '2015-12-26 16:21:42'),
(7, 3, 'Scooters', 'Multi Coloured Scooters print. 65% Cotton, 35% Polyester. Single jersey & stretchable. Beige', '3366-beige.png', 1850, 3, 3, 'A3366 Beige', '2015-12-29 18:30:57'),
(8, 1, 'Mini giraffes (Grey)', 'Loose fitting style, long sleeves.\r\nSize - One size ( fit UK size 8-16).\r\nFabric- 80% Cotton, 20% Polyester', '2725TGrey-600x860.png', 2300, 4, 2, '2725T Grey', '2015-12-29 19:36:22'),
(9, 1, 'Little Butterflies (Beige)', 'Feature- Baggy style, long sleeves, with multi-coloured little butterflies print.\r\nFabric- 80% Cotton, 20% Polyester', '2759TBeige.png', 1900, 0, 2, ' 2759T BEIGE', '2015-12-29 19:42:24'),
(10, 1, 'A girl with an umbrella (Grey)', 'Feature- Loose fitting style, long sleeves, comes in ''sweet girl with umbrella & dog in raining day'' print.  80% Cotton, 20% Polyester.', '2806TGrey.png', 2300, 0, 2, '2806T Grey', '2015-12-29 19:46:47'),
(11, 1, 'Shopping Girls (Beige)', 'Loose fitting style, long sleeves, with shopping girl cartoon and red dots print. 80% Cotton, 20% Polyester', '2970TBeige.png', 2300, 2, 2, '2970T Beige', '2015-12-29 19:50:18'),
(12, 1, 'Scooters (Beige)', 'Loose fitting style, long sleeves, with colourful scooters print over the jumper. 80% Cotton, 20% Polyester', '3366TBeige.png', 1800, 3, 2, '3366T Beige', '2015-12-29 19:53:00'),
(13, 2, 'Happy Big Owls (988 Black)', 'Bubble fitting style. Big Owls & Flower Print.\r\n35% Wool, 60% Polyester, 5% Elastane. \r\nSingle Jersey & stretchable', '988-Navy-tunic.png', 1900, 0, 5, '988 Black', '2015-12-29 20:01:44'),
(14, 2, 'Butterflies (Blue)', NULL, '7004-blue-tunic.png', 1900, 6, 6, '7004 blue', '2015-12-29 20:05:57'),
(15, 2, 'Fireworks Tunic', '35% Wool, 60% Polyester,5% Elastane, \r\nSingle jersey & stretchable.', '7008-black-tunic.png', 100, 3, 6, '7008', '2015-12-29 20:09:09'),
(16, 2, 'A flock of swallow birds', '35% Wool, 60% Polyester, 5% Elastane. \r\nSingle Jersey & stretchable', '7067-navy.png', 2400, 5, 6, '7067 navy', '2015-12-29 20:11:49'),
(17, 2, 'Pink Roses', NULL, '15010-navy-tunic.png', 1800, 4, 6, '15010 Navy', '2016-01-01 11:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `product_code`
--

CREATE TABLE IF NOT EXISTS `product_code` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `product_code`
--

INSERT INTO `product_code` (`id`, `name`) VALUES
(1, 'A2970 BEIGE'),
(2, 'A2759 Beige'),
(3, 'A2806 beige'),
(4, 'A2759 Beige'),
(5, 'A3366 Beige'),
(6, 'A3556 Beige');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_type` enum('coffee','goodies') DEFAULT NULL,
  `product_id` mediumint(8) unsigned NOT NULL,
  `price` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `start_date` (`start_date`),
  KEY `product_type` (`product_type`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `product_type`, `product_id`, `price`, `start_date`, `end_date`) VALUES
(1, 'goodies', 1, 1400, '2013-08-16', '2016-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `signups`
--

CREATE TABLE IF NOT EXISTS `signups` (
  `signups_id` int(10) NOT NULL AUTO_INCREMENT,
  `signup_email_address` varchar(250) DEFAULT NULL,
  `signup_date` date DEFAULT NULL,
  `signup_time` time DEFAULT NULL,
  PRIMARY KEY (`signups_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `signups`
--

INSERT INTO `signups` (`signups_id`, `signup_email_address`, `signup_date`, `signup_time`) VALUES
(1, 'xyz@yahoo.com', '2016-02-01', NULL),
(2, 'xxx@yahoo.com', '2016-02-13', '10:10:57'),
(3, 'xxxc@yahoo.com', '2016-02-13', '10:47:18'),
(4, 'xxw@yahoo.com', '2016-02-13', '10:48:18'),
(5, 'hg@yahoo.com', '2016-02-13', '13:17:52'),
(6, 'xcss@yahoo.com', '2016-02-14', '06:26:10'),
(7, 'sas@yahoo.com', '2016-02-14', '07:44:28'),
(8, 'new@yahoo.com', '2016-02-14', '10:18:14'),
(9, 'as@yahoo.com', '2016-02-14', '10:21:53'),
(10, 'asw@gmail.com', '2016-03-03', '14:54:31');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE IF NOT EXISTS `sizes` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `size` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `size` (`size`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`) VALUES
(2, 'M/L'),
(5, 'S-L-XL'),
(6, 'S-M-L-XL'),
(1, 'S/M'),
(3, 'SM - M/L'),
(4, 'XL');

-- --------------------------------------------------------

--
-- Table structure for table `wish_lists`
--

CREATE TABLE IF NOT EXISTS `wish_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_session_id` char(32) NOT NULL,
  `product_type` enum('coffee','goodies') DEFAULT NULL,
  `product_id` mediumint(8) unsigned NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `product_type` (`product_type`,`product_id`),
  KEY `user_session_id` (`user_session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
