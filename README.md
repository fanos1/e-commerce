# README #


### What is this repository for? ###

* This is a basic e-commerce website built with PHP-MYSQL. The Admin panel where the user can upload product is not yet finished. 
* visit www.dobaln.co.uk to see the live website which is built with this script.
* 1.0


### How do I get set up? ###

* To set up this script for your website first you need to create a database
* change the values of the "pdo-dobal.php" file to include your database connection credentials.
* "pdo-dobal.php" is outside the ROOT.
* Upload the "/html_public/" into your server's public dir.
* Go to your database and populate some testing data
* Upload some product images into your server's /images/ dir.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact