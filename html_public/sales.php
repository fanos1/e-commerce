<?php
require('./includes/config.inc.php');
include('./includes/product_functions.inc.php');

//$page_title = 'Sale Items';
//include('./includes/header.html');
//require(MYSQL);
//$r = mysqli_query($dbc, 'CALL select_sale_items(true)');

require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise</h3>");
}

/* 
if (mysqli_num_rows($r) > 0) {
    include('./views/list_sales.html');
} else {
    include('./views/noproducts.html');
}
 * 
 */


include(MODELS. 'Product.php');
$rows = Products::select_sale_items($dbc, TRUE);//@Param::TRUE fetchs All items

//---------- HTML ---------------
include(INCLUDES. 'header.php');
    
if ($rows) {    
    include ( VIEWS . "sales_view.php" );
    
} else {    
    //include ( VIEWS . "no_products_view.php" );
    echo "<h3>No products</h3>";
}

include(INCLUDES. 'footer.php');
?>