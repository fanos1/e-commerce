<?php 
// This file is the second step in the checkout process.

require('./includes/config.inc.php');
include('./includes/product_functions.inc.php');



// Start the session:
session_start();

// The session ID is the user's cart ID:
$uid = session_id();

// Check that this is valid:
if (!isset($_SESSION['customer_id'])) { // Redirect the user.
       
    $location = 'https://' . BASE_URL . 'checkout.php';
    header("Location: $location");
    exit();
}


//require(MYSQL);
require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise! str28</h3>");
}
include(MODELS. 'Cart.php');
include(MODELS. 'Order.php');

// For storing errors:
$billing_errors = array();



// Check for a form submission:
if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
    
    //We validate our own custom fields in the FORM such as 'first name' etc.. The fields which have "name" attribute
    //Stripe will validate the form's CVV, CARD NUMBER and ExPIREY DATE 
    
        
	if (get_magic_quotes_gpc()) {
		$_POST['cc_first_name'] = stripslashes($_POST['cc_first_name']);
		// Repeat for other variables that could be affected.
                $_POST['cc_last_name'] = stripslashes($_POST['cc_last_name']);
	}

	// Check for a first name:
	if (preg_match ('/^[A-Z \'.-]{2,20}$/i', $_POST['cc_first_name'])) {
		$cc_first_name = $_POST['cc_first_name'];
	} else {
		$billing_errors['cc_first_name'] = 'Please enter your first name!';
	}

	// Check for a last name:
	if (preg_match ('/^[A-Z \'.-]{2,40}$/i', $_POST['cc_last_name'])) {
		$cc_last_name  = $_POST['cc_last_name'];
	} else {
		$billing_errors['cc_last_name'] = 'Please enter your last name!';
	}	
	
	// Check for a street address:
	if (preg_match ('/^[A-Z0-9 \',.#-]{2,160}$/i', $_POST['cc_address'])) {
            $cc_address  = $_POST['cc_address'];
	} else {
            $billing_errors['cc_address'] = 'Please enter your street address!';
	}
		
	// Check for a city:
	if (preg_match ('/^[A-Z \'.-]{2,60}$/i', $_POST['cc_city'])) {
            $cc_city = $_POST['cc_city'];
	} else {
            $billing_errors['cc_city'] = 'Please enter your city!';
	}

       
	// Check for a zip code:
	//if (preg_match ('/^(\d{5}$)|(^\d{5}-\d{4})$/', $_POST['cc_zip'])) {
        if( preg_match('/^(?:[A-Za-z]\d ?\d[A-Za-z]{2})|(?:[A-Za-z][A-Za-z\d]\d ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d{2} ?\d[A-Za-z]{2})|(?:[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d[A-Za-z] ?\d[A-Za-z]{2})$/', $_POST['cc_zip'])) 
        {
		$cc_zip = $_POST['cc_zip'];
	} else {
		$billing_errors['cc_zip'] = 'Please enter your zip code!';
	}
        
        /* =============== WITH STRIPE WE DON'T STORE CREDIT CARD DETAIL ==================== Store 1234 as cc in table
       	// Check for a valid credit card number...
	// Strip out spaces or hyphens:
	$cc_number = str_replace(array(' ', '-'), '', $_POST['cc_number']);
	
	// Validate the card number against allowed types:
	if (!preg_match ('/^4[0-9]{12}(?:[0-9]{3})?$/', $cc_number) // Visa
	&& !preg_match ('/^5[1-5][0-9]{14}$/', $cc_number) // MasterCard
	&& !preg_match ('/^3[47][0-9]{13}$/', $cc_number) // American Express
	&& !preg_match ('/^6(?:011|5[0-9]{2})[0-9]{12}$/', $cc_number) // Discover
	) {
		$billing_errors['cc_number'] = 'Please enter your credit card number!';
	}
         * 
         */
        
        
        /* 
         * Check for a Stripe token: if no token returned from Stripe, this means stripe rejected our form inputs such as CVV
         * in that case, Add error to $billing_errors.
         * POST['token'] is not in our FORM, it's returned from stripe as hidden field after is sent to Stripe via Ajax
        */
        if(isset($_POST['token'])) { 
            $token = $_POST['token'];		            
        } 
        elseif (isset($_POST['stripeToken']) )  //Stripe could alos send $_POST['stripeToken'] instead of $_POST['token']
        {
            $token = $_POST['stripeToken'];
        }
        else {        
            $message = 'The order cannot be processed. Please make sure you have JavaScript enabled and try again, str105';        
            $billing_errors['token'] = TRUE; //We set $billing_errors to TRUE if stipe did not return token; When a var has TRUE value, empty() returns false                
        }
        
        
        //=======================
        // If everything's OK...        
        // IF All inputs are valid, AND Stripe returned 'token', continue          
        //===========================
	if (empty($billing_errors))   // $billing_errors was to TRUE if stipe did not return token; When a var has TRUE value, empty() returns false 
	{

		// Check for an existing order ID:
		if (isset($_SESSION['order_id'])) { // Use existing order info:
			$order_id = $_SESSION['order_id'];
			$order_total = $_SESSION['order_total'];
                        
		} else { // Create a new order record:

			// Get the last four digits of the credit card number:
			// Temporary solution for Stripe:
			$cc_last_four = 1234;
			//$cc_last_four = substr($cc_number, -4);
                        
                        //WITH STRIPE WE DON'T NEED TO STORE CREDIT CARD INFORMATION 

			// Call the stored procedure:
			$shipping = $_SESSION['shipping'] * 100;
			//$r = mysqli_query($dbc, "CALL add_order({$_SESSION['customer_id']}, '$uid', $shipping, $cc_last_four, @total, @oid)");
                        
                         
                        //add_order() Returns the 'LastOrderID'
                        $r = Order::add_order($dbc, $_SESSION['customer_id'], $uid, $shipping, $cc_last_four);
                        
                        if( is_array($r) && !empty($r) ) { //if the add_order() returned an Array, not empty                            
                                                        
                            list($order_id, $order_total) = $r;
                             
                             $_SESSION['order_total'] = $order_total;
                             $_SESSION['order_id'] = $order_id;                                                          
                             
                        } else { // The add_order() method failed for some reason. it could be that this method couln not retrieve the order ID and total.                            
                            unset($cc_number, $cc_cvv, $_POST['cc_number'], $_POST['cc_cvv']);
                            trigger_error('Your order could not be processed due to a system error. We apologize for the inconvenience.');
                        } 
                        
                        /* 
			// Confirm that it worked:
			if ($r) {                            
                            // Retrieve the order ID and total:
                            $r = mysqli_query($dbc, 'SELECT @total, @oid');
                            
                            if (mysqli_num_rows($r) == 1) {
                                list($order_total, $order_id) = mysqli_fetch_array($r);
                                // Store the information in the session:
                                $_SESSION['order_total'] = $order_total;
                                $_SESSION['order_id'] = $order_id;
                            } else { // Could not retrieve the order ID and total.
                                unset($cc_number, $cc_cvv, $_POST['cc_number'], $_POST['cc_cvv']);
                                trigger_error('Your order could not be processed due to a system error. We apologize for the inconvenience.');
                            }
                                
			} else { // The add_order() procedure failed.
                            trigger_error('Your order could not be processed due to a system error. We apologize for the inconvenience.');
			}
                         */
                         
		}
		
		
		//------------------------
		// Process the payment!
		//---------------------
		if (isset($order_id, $order_total)) {

			try {
                           
                            // Include the Stripe library:
                            //require_once('includes/Stripe.php');

                            
                            require_once './includes/stripe-php-3.6.0/init.php';
                            
                            
                            
                            //require_once './includes/stripe-php-3.6.0/lib/Stripe.php';
                            
                            //IN NEW VERSION OF STRIPE:: The PHP library is installed via Composer. 
                            //Simply add "stripe/stripe-php" to your composer.json file:

                            // set your secret key: remember to change this to your live secret key in production
                            // see your keys here https://manage.stripe.com/account
                            ///Stripe::setApiKey('sk_test_cyFiWIBiAhaZiW2WiURm4MNw');
                            
                            
                            //Stripe::setApiKey('sk_test_eYJO5FcU2jtUaPS1ozXMqmD9'); //My Test key
                            
                            /* ========== TEST KEY ===============
                            \Stripe\Stripe::setApiKey('sk_test_eYJO5FcU2jtUaPS1ozXMqmD9');  //TEST KEY
                             * 
                             */
                            
                            \Stripe\Stripe::setApiKey('sk_live_K2i5qtGtbGd67e40Anpw0ORW'); //LIVE KEY
                            


                            // Charge the order:
                            // Up to this point, we only generated a token that represented the credit card data.
                            //$charge = Stripe_Charge::create(array(
                            //$charge = \Stripe\Charge::create(...);
                            $charge = \Stripe\Charge::create(array(
                                    'amount' => $order_total,
                                    'currency' => 'gbp',
                                    'card' => $token,
                                    'description' => $_SESSION['email'],
                                    'capture' => false
                                    )
                            );

                            //echo '<pre>' . print_r($charge, 1) . '</pre>';exit;

                            // Did it work?
                            if ($charge->paid == 1) {

                                
                                // Add slashes to two text values:
                                $full_response = addslashes(serialize($charge));

                              
                                
                                // Record the transaction:
                                // $r = mysqli_query($dbc, "CALL add_charge('{$charge->id}', $order_id, 'auth_only', $order_total, '$full_response')");				
                                $r = Order::add_charge($dbc, $charge->id, $order_id, 'auth_only', $order_total, $full_response);

                                //if($r) {
                                //    echo 'TRUE';
                                //}
                                
                                
                                // Add the transaction info to the session:
                                $_SESSION['response_code'] = $charge->paid;

                                
                                
                                // Redirect to the next page:
                                $location = 'https://' . BASE_URL . 'final.php';
                                header("Location: $location");
                                exit();

                            } else { // Charge was not paid!	
                                $message = $charge->response_reason_text;
                                //echo "<h3>268</h3>";
                                //echo $message;
                            }

			} catch (Stripe_CardError $e) { // Stripe declined the charge.
                            
                            $e_json = $e->getJsonBody();
                            $err = $e_json['error'];
                            $message = $err['message'];
                            //echo '268' . $message;
                            
			} catch (Exception $e) { // Try block failed somewhere else.
                            trigger_error(print_r($e, 1));
                            //echo '274';
			}

		} // End of isset($order_id, $order_total) IF.
                
                
	} // Errors occurred IF

} // End of REQUEST_METHOD IF.



//============== HTML ==============				
//============== HTML ==============
//============== HTML ==============
// Include the header file:
$page_title = 'Dobaln Fashion - Checkout - Your Billing Information';
include('./includes/header_checkout.php');

$rows = Cart::get_shopping_cart_contents($dbc, $uid);//SHOPPING CART

if($rows) {
    
    if (isset($_SESSION['shipping_for_billing']) && ($_SERVER['REQUEST_METHOD'] !== 'POST')) {
        $values = 'SESSION';
    } else {
        $values = 'POST';
    }    
    include('./views/billing_stripe_view.php');
    
} else { // Empty cart!
    include('./views/emptycart.php');
}

// Finish the page:
include('./includes/footer.php');
?>