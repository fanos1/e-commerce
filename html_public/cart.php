<?php
require('./includes/config.inc.php');
include('./includes/product_functions.inc.php');

// Check for, or create, a user session:
if (isset($_COOKIE['SESSION']) && (strlen($_COOKIE['SESSION']) === 32)) {
    $uid = $_COOKIE['SESSION'];
} else {
    $uid = openssl_random_pseudo_bytes(16);
    $uid = bin2hex($uid);
}


setcookie('SESSION', $uid, time()+(60*60*24));// keep cookie 1 day


require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise</h3>");
}
include(MODELS. 'Cart.php');


// If there's a SKU value in the URL, break it down into its parts:
if (isset($_GET['sku'])) {    
    list($type, $pid) = parse_sku($_GET['sku']);    
}


$msg = '';//var which displays message to user if product was successfully added or removed to Cart

if (isset($pid, $type, $_GET['action'], $_GET['size']) && ($_GET['action'] === 'add') ) {   
    
    //VALIDATE THE GET[size]
    if( !preg_match('/^[a-zA-Z0-9\/]{1,20}$/', $_GET['size']) ) { //if FALSE, exit
        exit('An error occured, we apoligize!');
    } else {
        $size = trim($_GET['size']);
    }
    

    //$r = mysqli_query($dbc, "CALL add_to_cart('$uid', '$type', $pid, 1)");
    // if (!$r) echo mysqli_error($dbc);// For debugging purposes:        
    
    $rows = Cart::add_to_cart($dbc, $uid, $type, $pid, 1, $size);
    
    if(!$rows) {
        echo "<h3>item not added successfully</h3>";
        exit('cik,str63');
    } 
		
} elseif (isset($type, $pid, $_GET['action']) && ($_GET['action'] === 'remove') ) {
	
    //$r = mysqli_query($dbc, "CALL remove_from_cart('$uid', '$type', $pid)");    
    $rows = Cart::remove_from_cart($dbc, $uid, $type, $pid);
    
    if($rows) {        
        $msg = 'item removed successfully from cart';            
    } else {
        $msg = 'item could not be removed from the shopping cart';            
    }
    

} elseif (isset($type, $pid, $_GET['action'], $_GET['qty']) && ($_GET['action'] === 'move') ) {
    
        
    $r = Cart::add_to_cart($dbc, $uid, $sp_type, $pid, $qty);
    
    if($r) {
        $msg = 'item moved successfully to cart';         
    } 
    
    
    //Remove from wish list
    $r = Cart::remove_from_wish_list($dbc, $uid,  $sp_type, $pid);
    
    if($r) {            
        $msg = 'item removed successfully from wish list';         
    } 
    
    

} elseif (isset($_POST['quantity'])) { // Update quantities in the cart.
    
    // Loop through each item:
    foreach ($_POST['quantity'] as $sku => $qty) { 		

        list($type, $pid) = parse_sku($sku);// Parse the SKU:

        if (isset($type, $pid)) {
            // Determine the quantity:
            $qty = (filter_var($qty, FILTER_VALIDATE_INT, array('min_range' => 0)) !== false) ? $qty : 1;
            // Update the quantity in the cart:
            //:::$r = mysqli_query($dbc, "CALL update_cart('$uid', '$type', $pid, $qty)");
            
            $r = Cart::update_cart($dbc, $uid, $type, $pid, $qty);
            /* 
            if($r) {
                //echo "<h3>item updated in the cart</h3>";
                //$itemUpdated = '<div class="alert alert-success"> <strong>Success!</strong> Indicates a successful or positive action.</div>';
            } else {
                echo "<h3>item NOT updated in the cart str117</h3>";
            }
            */
            
        }

    } 	
}


 

//=============== HTML =====================
//=============== HTML =====================
//=============== HTML =====================
include(INCLUDES. 'header.php');
    
$rows = Cart::get_shopping_cart_contents($dbc, $uid);

if($rows) {        
    
    include ( VIEWS . "cart_view.php" );     
    
    if( isset($itemRemoved) ) {
        echo '
            <div class="container">
                <div class="row">
                    <div class="alert alert-success">'. $msg . '</div>
                </div>
            </div>';
    }
} else {    
    include ( VIEWS . "emptycart_view.php" );        
}
 
include(INCLUDES. 'footer.php');
?>