<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="description" content="Dobaln is boutique Ladies fashion business in East London." /> 
    <meta name="keywords" content="dobaln fashion, boutique ladies fashion shop, East London" /> 
    <meta name="author" content="irfan" /> 
    
    
    <title><?php // Use a default page title if one wasn't provided...
        if (isset($page_title)) { 
            echo $page_title; 
        } else { 
            echo 'Dobaln | boutique fashion clothing shop East London'; 
        } 
        ?>
    </title>
     
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/css/custom.css" rel="stylesheet" /> 
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
   
    <!--[if lt IE 7]>
       <script type="text/javascript" src="/js/ie_png.js"></script>
       <script type="text/javascript">
           ie_png.fix('.png, .logo h1, .box .left-top-corner, .box .right-top-corner, .box .left-bot-corner, .box .right-bot-corner, .box .border-left, .box .border-right, .box .border-top, .box .border-bot, .box .inner, .special dd, #contacts-form input, #contacts-form textarea');
       </script>
    <![endif]-->
</head>

<body>
   
    <!-- commonlinks bar -->
   <div class="container"  style="padding:1em 2em;">
    	<div class="row">
        
            <div class="col-lg-6">
            	<span style="font-size:20px;" title="logo">Dobaln</span>
                
                <a href="http://<?php echo BASE_URL; ?>/cart.php">
                    <span class="glyphicon glyphicon-shopping-cart pull-right" style="color:#FF1493;"></span>
                    <span class="sr-only">(Cart)</span>
                </a>                
            </div>
            
            <div class="col-lg-6" style="font-size:small;">            	
            	<div class="pull-right">
                    	<span class="glyphicon glyphicon-earphone"></span>
	                <span>+447914014749</span>

                        <span class="glyphicon glyphicon-envelope" style="color:#FF1493;"></span>
                        <span>dobalnfashion@gmail.com</span>                               
                </div>				
            </div>
        </div>
    </div><!-- container -->
    
    
       
    <!-- NAVIGATION -->
    <div class="container" style="margin-top:1em;">                        
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>                  
                </div>


                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">                        
                        <li>
                            <a href="http://www.dobaln.co.uk">
                                <span class="glyphicon glyphicon-triangle-left"></span> HOME
                            </a>
                        </li>
                    </ul>                    
                    

                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

    </div>
    

    