<?php

// This script sends a receipt out in HTML format.

$to = $_SESSION['email'];
$to .= ',secondemail@yahoo.com';

$subject = "Your Order";


$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>    
";

$body_html .=  '
<p>Thank you for your order. Your order number is ' . $_SESSION['order_id'] . 
    '. All orders are processed on the next business day. You will be contacted in case of any delays.</p>
    <table border="0" cellspacing="3" cellpadding="3">
	<tr>
		<th align="center">Item</th>
		<th align="center">Quantity</th>
		<th align="right">Price</th>
		<th align="right">Subtotal</th>
	</tr>';

// Get the cart contents for the confirmation email:
//$r = mysqli_query($dbc, "CALL get_order_contents({$_SESSION['order_id']})");
$row = Order::get_order_contents($dbc, $_SESSION['order_id'] );



foreach ($row as $array) {
    
    // Add to the HTML:
    $body_html .= '<tr><td>' . $array['category'] . '-' . $array['name'] . '</td> 
            <td align="center">' . $array['quantity'] . '</td>
            <td align="right">£' . number_format($array['price_per']/100, 2) . '</td>
            <td align="right">£' . number_format($array['subtotal']/100, 2) . '</td>
    </tr>
    ';

    // For reference after the loop:
    $shipping = number_format($array['shipping']/100, 2);
    $total = number_format($array['total']/100, 2);

    
}

$body_html .= '<tr>
	<td colspan="2"> </td><th align="right">Shipping</th>
	<td align="right">£' . $row[0]['shipping'] . '</td>
</tr>
'; 


// Add the total:
$body_html .= '<tr>
	<td colspan="2"> </td><th align="right">Total</th>
	<td align="right">£' . $total . '</td>
    </tr>
</table>
';


$message .= $body_html; //Add the order html to message

$message .= '</body></html>'; //close the <html>


$headers = 'From: info@xxx.com' . "\r\n" .
        'Reply-To: xxxyyy@gmail.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

$headers .= "cc:irfan@yahoo.com \r\n";	
// Always set content-type when sending HTML email
$headers .= "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

mail($to,$subject,$message,$headers);

