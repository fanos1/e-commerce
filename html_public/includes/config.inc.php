<?php

// Are we live?
if (!defined('LIVE')) DEFINE('LIVE', TRUE);

$live = FALSE;


// Errors are emailed here:
DEFINE('CONTACT_EMAIL', 'webmaster@xx.co.uk');


define ('BASE_URI', '/home/xxx/'); 

define ('BASE_URL', 'www.example.co.uk/');
define ('PDO', BASE_URI . 'pdo-dobal.php');

define ('ROOT', $_SERVER['DOCUMENT_ROOT']); 
define ('VIEWS', ROOT.'/views/');
define ('MODELS', ROOT.'/models/');
define ('INCLUDES', ROOT.'/includes/');

define ('IMAGE_DIR', ROOT.'/images/');
define ('ROOT_HTTP', 'http://www.example.co.uk/');
define ('ROOT_HTTPS', 'https://www.example.co.uk/');



// Function for handling errors.
// Takes five arguments: error number, error message (string), name of the file where the error occurred (string) 
// line number where the error occurred, and the variables that existed at the time (array).
// Returns true.
function my_error_handler($e_number, $e_message, $e_file, $e_line, $e_vars) {
    
    global $live;
    
	$message = "An error occurred in script '$e_file' on line $e_line:\n$e_message\n";
	
	// Add the backtrace:
	$message .= "<pre>" .print_r(debug_backtrace(), 1) . "</pre>\n";
	
	// Or just append $e_vars to the message:
	//	$message .= "<pre>" . print_r ($e_vars, 1) . "</pre>\n";

	if (!LIVE) { // Show the error in the browser.
        //if (!$live) { // Show the error in the browser.
            
            echo '<div class="alert alert-danger">' . nl2br($message) . '</div>';
            
	} else { // Development (print the error to log file).

            // Send the error in an email:
            error_log ($message, 1, CONTACT_EMAIL, 'From:webmaster@xxr.co.uk');

            // Only print an error message in the browser, if the error isn't a notice:
            if ($e_number != E_NOTICE) {
                    echo '<div class="alert alert-danger">A system error occurred, irf. We apologize for the inconvenience.</div>';
                    
            } 

	} // End of $live IF-ELSE.
	
	return true; // So that PHP doesn't try to handle the error, too.

} 
// Use my error handler:
set_error_handler('my_error_handler');




/* 
* PDO Uncaught Exception Handler
* -----------------------------
* PHP Master: Write Cutting Edge Code Paperback – 4 Nov 2011
*/
function handleMissedException($e) {
	echo "Sorry, something is wrong. Please try again, or contact us.if the problem persists";
	error_log('Unhandled Exception Sam: ' . $e->getMessage() . ' in file ' . $e->getFile() . ' on line ' . $e->getLine());
}
set_exception_handler('handleMissedException');




//function to format date fetched from DB
function formatDate($param) {            
    //$param == '2013-07-29 14:35:09'
    //split string from space to get date and time seperate    
    $pieces = explode(' ', $param);    
    return $pieces;
}


