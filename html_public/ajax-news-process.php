<?php
require('./includes/config.inc.php');

require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise</h3>");
}


if($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    //anything echo ed is returned to javascript    :: you can echo json() encoded  or just norma
    //echo json_encode( $_POST['bar'] );
    //echo $_POST['bar'];
    
    /* 
    //return json response
    $data = array(
        'status' => 'success',
        'message' => 'mesaj'
    );   
    */
    
    
    // Remove all illegal characters from email
    $email = filter_var($_POST['bar'], FILTER_SANITIZE_EMAIL);
    
    if(empty($email)){
        $status = "error";
        $message = "You did not enter an email address!";
    }
    else if(!preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/', $email)){ //validate email address - check if is a valid email address
        $status = "error";
        $message = "You have entered an invalid email address!";
    }
    else {
        
        try {
        
            $query = "SELECT signup_email_address FROM signups WHERE signup_email_address=:email ";
            $stmt = $dbc->prepare($query);
            $stmt->bindParam(':email', $email);   
            
            if( $stmt->execute() ) { //Execute
                $rows = $stmt->fetch(PDO::FETCH_ASSOC);
            }
            
            
            if( !$rows['signup_email_address'] ) { //FALSE is returned if no record in database, which means this is available, we can insert

                $date = date('Y-m-d');
                $time = date('H:i:s');

                $q2 = "
                INSERT INTO signups(signup_email_address, signup_date, signup_time)
                VALUES(:email, :date, :time)
                ";				
                $stmt = $dbc->prepare($q2); 
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':date', $date);
                $stmt->bindParam(':time', $time);

                if($stmt->execute() ) {
                    $status = "success";
                    $message = "You have been signed up!"; 
                } else {
                    $status = "error";
                    $message = "Theres been a technical error!"; 
                }

            } else { //already signed up
                $status = "error";
                $message = "This email address has already been registered!";
            }
            
        } catch (Exception $ex) {            
            //echo $ex->getMessage();
            $message = $ex->getMessage();
            $status = "error";
        }
      
    }
         
    //return json response
    $data = array(
        'status' => $status,
        'message' => $message
    );
     
    
    //Once the data has been encoded as JSON, we need to return it back to the clients browser. We do this with a simple echo statement.
    echo json_encode($data);
    exit;
       
}


//=========== TESTIGN =====================
/* 
if($_SERVER['REQUEST_METHOD'] === 'GET') {
    
    echo '<h1>'. $_GET['bar'] .'</h1>';
    
    // Remove all illegal characters from email
    $email = filter_var($_GET['bar'], FILTER_SANITIZE_EMAIL);
    
    if(empty($email)){
        $status = "error";
        $message = "You did not enter an email address!";
        echo '1 <br />';
    }
    else if(!preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/', $email)){ //validate email address - check if is a valid email address
        $status = "error";
        $message = "You have entered an invalid email address!";
        echo '2 <br />';
    }
    else {
        
        try {
        
            $query = "SELECT signup_email_address FROM signups WHERE signup_email_address=:email ";
            $stmt = $dbc->prepare($query);
            $stmt->bindParam(':email', $email);   
            
            if( $stmt->execute() ) { //Execute
                $rows = $stmt->fetch(PDO::FETCH_ASSOC);
            }
            
            echo count($rows) .'<br />';
            
            //var_dump($rows);
            //exit();
            
            
            if(! $rows['signup_email_address'] ) { // if no record in database, FALSE returned

                $date = date('Y-m-d');
                $time = date('H:i:s');

                $q2 = "
                INSERT INTO signups(signup_email_address, signup_date, signup_time)
                VALUES(:email, :date, :time)
                ";				
                $stmt = $dbc->prepare($q2); 
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':date', $date);
                $stmt->bindParam(':time', $time);

                if($stmt->execute() ) {
                    $status = "success";
                    $message = "You have been signed up!"; 
                } else {
                    $status = "error";
                    $message = "Ooops, Theres been a technical error!"; 
                }

            } else { //already signed up
                $status = "error";
                $message = "This email address has already been registered!";
            }
            
        } catch (Exception $ex) {            
            //echo $ex->getMessage();
            $message = $ex->getMessage();
            $status = "error";
        }
      
    }
    
    echo "<h3>$status</h3>";
    echo "<h3>$message</h3>";
    exit();
    
    //return json response
    $data = array(
        'status' => $status,
        'message' => $message
    );
    
    //Once the data has been encoded as JSON, we need to return it back to the clients browser. We do this with a simple echo statement.
    echo json_encode($data);
    exit;
    
} 
 * 
 */
 