<?php

require('./includes/config.inc.php');

if (!isset($_GET['x'], $_GET['y']) || !filter_var($_GET['x'], FILTER_VALIDATE_INT,  array('min_range' => 1)) || (strlen($_GET['y']) !== 40) ) { // Redirect the user.
    $location = 'https://' . BASE_URL . 'index.php';
    header("Location: $location");
    exit();
} else {
    $order_id = $_GET['x'];
    $email_hash = $_GET['y'];
}

//require(MYSQL);
require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise</h3>");
}
include(MODELS. 'Cart.php');
include(MODELS. 'Order.php');


// Set the page title and include the header:
include('./includes/header_checkout.php');



//OO PHP
try {
    
    /* 
    $q = '
    SELECT FORMAT(total/100, 2) AS total, 
    FORMAT(shipping/100,2) AS shipping, 
    credit_card_number, 
    DATE_FORMAT(order_date, "%a %b %e, %Y at %h:%i%p") AS od, 
    email, 
    CONCAT(last_name, ", ", first_name) AS name, 
    CONCAT_WS(" ", address1, address2, city, state, zip) AS address, 
    phone, 
    CONCAT_WS(" - ", ncc.category, ncp.name) AS item, 
    quantity, 
    FORMAT(price_per/100,2) AS price_per 
    FROM orders AS o 
    INNER JOIN customers AS c ON (o.customer_id = c.id) 
    INNER JOIN order_contents AS oc ON (oc.order_id = o.id) 
    INNER JOIN products AS ncp ON (oc.product_id = ncp.id AND oc.product_type="goodies") 
    INNER JOIN categories AS ncc ON (ncc.id = ncp.non_coffee_category_id) 
    WHERE o.id=:oid AND SHA1(email)=:email_hash
    '; 
     * 
     */
     
     
    $q = '
    SELECT FORMAT(total/100, 2) AS total, 
    FORMAT(shipping/100,2) AS shipping, 
    credit_card_number, 
    DATE_FORMAT(order_date, "%a %b %e, %Y at %h:%i%p") AS od, 
    email, 
    CONCAT(last_name, ", ", first_name) AS name, 
    CONCAT_WS(" ", address1, address2, city, zip) AS address, 
    phone, 
    CONCAT_WS(" - ", ncc.category, ncp.name) AS item, 
    quantity, 
    FORMAT(price_per/100,2) AS price_per 
    FROM orders AS o 
    INNER JOIN customers AS c ON (o.customer_id = c.id) 
    INNER JOIN order_contents AS oc ON (oc.order_id = o.id) 
    INNER JOIN products AS ncp ON (oc.product_id = ncp.id AND oc.product_type="goodies") 
    INNER JOIN categories AS ncc ON (ncc.id = ncp.non_coffee_category_id) 
    WHERE o.id=:oid
    ';
     
    
     
    /* 
    array(1) {  
        [0] =>  array(22) {
            ["total"]	=>    string(4) "5.00"
            [0]			=>    string(4) "5.00"
            ["shipping"]=>    string(4) "0.00"
            [1]			=>    string(4) "0.00"    
                ["credit_card_number"]=>   string(4) "1234"
            [2]			=>    string(4) "1234"
            ["od"]		=>    string(27) "Sat Jan 23, 2016 at 02:09PM"
            [3]			=>    string(27) "Sat Jan 23, 2016 at 02:09PM"
            ["email"]	=>    string(22) "irfankissa@outlook.com"
            [4]=>    string(22) "irfankissa@outlook.com"
            ["name"]	=>    string(18) "lksdfasld, sdkfasl"
            [5]=>    string(18) "lksdfasld, sdkfasl"
            ["address"]	=>    string(25) "sdfasfd london AL n16 5hz"
            [6]=>    string(25) "sdfasfd london AL n16 5hz"
            ["phone"]	=>    string(10) "0791401474"
            [7]=>    string(10) "0791401474"
            ["item"]	=>    string(30) "Cardigans - Little Butterflies"
            [8]			=>    string(30) "Cardigans - Little Butterflies"
            ["quantity"]=>    string(1) "1"
            [9]			=>    string(1) "1"
            ["price_per"]=>    string(4) "5.00"
            [10]		=>    string(4) "5.00"
          }
        }
     * ----------------------------------------------------    
     */
    
     
     
    
    $stmt = $dbc->prepare($q);            						
    $stmt->bindParam(':oid', $order_id);			
    //$stmt->bindParam(':email_hash', $email_hash);			
    $stmt->execute();
    //$r = $stmt->fetch(PDO::FETCH_ASSOC);
    $r = $stmt->fetchAll();
    
    
    //var_dump($r);
    
    $output = '';
    
    // Display the order and customer information:       
    $output .= '<p><strong>Order ID</strong>: ' . $order_id . '</p>
            <p><strong>Order Date</strong>: ' . $r[0]['od'] . '</p>
            <p><strong>Customer Name</strong>: ' . htmlspecialchars($r[0]['name']) . '</p>
            <p><strong>Shipping Address</strong>: ' . htmlspecialchars($r[0]['address']) . '</p>
            <p><strong>Customer Email</strong>: ' . htmlspecialchars($r[0]['email']) . '</p>
            <p><strong>Customer Phone</strong>: ' . htmlspecialchars($r[0]['phone']) . '</p>';
    
    
    //Table
    $output .= '<table class="table">
    <thead>
        <tr>
            <th>Item</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Subtotal</th>
      </tr>
    </thead>
    <tbody>';
    
    
    foreach ($r as $k=>$array) {
        
        $output .= '<tr>
            <td>' . $array['item'] . '</td>
            <td>' . $array['quantity'] . '</td>
            <td>£' . $array['price_per'] . '</td>
            <td>£' . number_format($array['price_per'] * $array['quantity'], 2) . '</td>
        </tr>';
    }
    
    
    // Show the shipping and total:    
    $output .= '<tr>
        <td colspan="3"><strong>Shipping</strong></td>
        <td>£' . $r[0]['shipping'] . '</td>
    </tr>';
    
    $output .= '<tr>
        <td colspan="3"><strong>Total</strong></td>
        <td>£' . $r[0]['total'] . '</td>
    </tr>';
            

    // Complete the table and the form:
    //echo '</tbody></table>';
    $output .= '</tbody></table>';
    

} catch (Exception $ex) {
    //echo 'An Error occured, we apologize! str254';
}
?>

<div class="container" style="padding-left: 1em; ">
    <div class="row">
        <div class="col-md-12">
            <?php echo $output; ?>
        </div>
    </div>    
</div>


</body>
</html>