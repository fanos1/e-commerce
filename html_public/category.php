<?php
require('./includes/config.inc.php');
include('./includes/product_functions.inc.php');// Need the utility functions:


// Validate the required values:
$type = $sp_cat = $category = false;

if (isset($_GET['type'], $_GET['category'], $_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' => 1))) {
	
    // Make the associations:
    $category = $_GET['category'];
    $sp_cat = $_GET['id'];

    
    //if ($_GET['type'] === 'goodies' ) {		
    if ($_GET['type'] === 'cardigans' || $_GET['type'] === 'jumpers' || $_GET['type'] === 'tunics' ) {		    
        $type = 'goodies';		
    } elseif ($_GET['type'] === 'coffee') {		
        $type = 'coffee';			
    }

}

// If there's a problem, display the error page:
if (!$type || !$sp_cat || !$category) {
    $page_title = 'Error!';        
    include(INCLUDES. 'header.php');
    include ( VIEWS . "error_view.php" ); 
    include(INCLUDES. 'footer.php');
    exit();
}

//$page_title = ucfirst($type) . ' to Buy::' . $category;
$page_title = 'Ladies clothes on sale - ' . $category;

require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise</h3>");
}

include(MODELS. 'Product.php');
$rows = Products::select_products($dbc, $type, $sp_cat);




// ========== HTML ==============
// ========== HTML ==============
include('./includes/header.php');
if ($rows) {   
    
     if ($type === 'goodies') {     
         
        include ( VIEWS . "category_view.php" );   
        
         
    } elseif ($type === 'coffee') {
           
        //include('./views/list_coffees2.html');
        include ( VIEWS . "coffees_view.php" ); 
        /* 
        // Clear the stored procedure results:
        mysqli_next_result($dbc);    
                  
        // Handle and show reviews...  
        include('./includes/handle_review.php');

        $r = mysqli_query($dbc, "CALL select_reviews('$type', $sp_cat)");
        include('./views/review.html'); 
         * 
         */
        
        //mysqli_next_result($link) :: Prepare next result from multi_query

    }
    
} else {
    include ( VIEWS . "noproducts_view.php" ); 
}

// Include the footer file:
include('./includes/footer.php');
?>