<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php

//Solution taken from: http://stackoverflow.com/questions/15960184/united-kingdom-gb-postal-code-validation-without-regex
//When checking on the validation part, it seems that there are 6 type of formats (A = letter and 9 = digit):
/* 
=================================================================== 
AA9A 9AA                       AA9A9AA                   AA9A9AA
A9A 9AA     Removing space     A9A9AA       order it     AA999AA
A9 9AA    ------------------>  A99AA     ------------->  AA99AA
A99 9AA                        A999AA                    A9A9AA
AA9 9AA                        AA99AA                    A999AA
AA99 9AA                       AA999AA                   A99AA
=================================================================== 

As we can see, the length may vary from 5 to 7 and we have to take in account some special cases if we want to.
So the function we are coding has to do the following:


1    Remove spaces and convert to uppercase (or lower case).
2    Check if the input is an exception, if it is it should return valid
3    Check if the input's length is 4 < length < 8.
4    Check if it's a valid postcode.


The last part is tricky, but we will split it in 3 sections by length for some overview:
1    Length = 7: AA9A9AA and AA999AA
2    Length = 6: AA99AA, A9A9AA and A999AA
3    Length = 5: A99AA


*/


//a regular expression found on : http://stackoverflow.com/questions/164979/uk-postcode-regex-comprehensive
/*
	---------------------------------------------------------------------
	| format 	|	coverage 								| Example   |
	---------------------------------------------------------------------
	AA9A 9AA	| WC postcode area: EC1-EC4, NW1W, SE1P		| EC1A 1BB  |
	---------------------------------------------------------------------
	A9A 9AA		| E1W, N1C, N1P								| W1A OAX   |
	---------------------------------------------------------------------
	A9 9AA		| 											| M1 1AE    |
					B,E,G,L,M,N,S,W							
	A99 9AA		| 											| B33 8TH	|
	---------------------------------------------------------------------
	AA9 9AA		| 											| CR2 6XH	|
					All other postcodes
	AA99 9AA	| 											| DN55 1PT	|
	---------------------------------------------------------------------
	
*/

if( preg_match('/^(?:[A-Za-z]\d ?\d[A-Za-z]{2})|(?:[A-Za-z][A-Za-z\d]\d ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d{2} ?\d[A-Za-z]{2})|(?:[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d[A-Za-z] ?\d[A-Za-z]{2})$/', 'N16 5HZ') ) {
	echo 'TRUE postcode';
}



?>
</body>
</html>