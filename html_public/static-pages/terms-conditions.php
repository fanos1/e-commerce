
<?php 
require('../includes/config.inc.php');

include(INCLUDES.'header.php'); 
?>


<div class="container" style="padding:2em;">
    <div class="row">
        <div class="col-lg-12">
            <h1>Our Privacty Policy</h1>
            <p>
                Welcome to the Dobaln Fashion.co.uk Web Site (the "Site"). Dobaln Fashion Limited (now referred to as "Dobaln Fashion")
                provides this Site as a service to its customers. Please review the following basic rules that govern your
                 use of our Site (the "Agreement"). Please note that your use of our Site constitutes your agreement to follow 
                 and be bound by these terms. If you do not agree to these terms, please do not use this Site.
            </p>
            <p>                
                Although you may "bookmark" a particular portion of this Site and thereby bypass this Agreement, your use of this 
                Site still binds you to the terms. Dobaln Fashion reserves the right to update or modify these Terms and Conditions at any 
                time without prior notice. Your use of the Dobaln Fashion.co.uk web site following any such changes constitutes your agreement 
                to follow and be bound by the Terms and Conditions as changed. For this reason, we encourage you to review these Terms and 
                Conditions whenever you use this web site.
            </p>
            <h2>USAGE RESTRICTIONS</h2>
            <p>
                All of the content you see and hear on the Dobaln Fashion.co.uk web site, including, for example, all of the page headers, images, illustrations, graphics, audio clips, video clips, and text are subject to trademark, service mark, trade dress, copyright and/or other intellectual property rights or licenses held by Dobaln Fashion, one of its affiliates or by third parties who have licensed their materials to Dobaln Fashion. The entire content of the Dobaln Fashion.co.ukweb site is copyrighted as a collective work under U.K. copyright laws, and coordination, arrangement and enhancement of the content.
            </p>
            <p>
                The content of the Dobaln Fashion.co.uk web site, and the site as a whole, are intended solely for the personal, noncommercial use by the users of our site. You may download, print or store selected portions of the content, provided you (1) only use these materials for your own personal, noncommercial use, (2) do not copy or post the content on any network computer or broadcast the content in any media, and (3) do not modify or alter the content in any way, or delete or change any copyright or trademark notice.
            </p>
            <p> 
                No right, title or interest in any content or materials is transferred to you as a result of any such activities. Dobaln Fashion reserves complete title and full intellectual property rights in any content you download, reproduce, print, redistribute or store from this web site.
            </p>
            
            <h2>COLORS</h2> 
            <p>
                We have done our best to display as accurately as possible the colors of the products shown on the Dobaln Fashion.co.uk web site. However, because the colors you see will depend on your monitor, we cannot guarantee that your monitor's display of any color will be accurate.
            </p>
            
            <h2>DISCLAIMER </h2>
            <p>                
               this site and the materials and products in this site are provided "as is" and without warranties of any kind, whether express or implied. to the fullest extent permissible pursuant to applicable law, purplish disclaims warranties, express or implied, including but not limited to, implied warranties of merchantability and fitness for a particular purpose and non-infringemnt. purplish does not represent or warrant that the functions contained in the site will be uninterrupted or error-free, that the defects will be corrected, or that this site or the server that makes the site available are free of viruses or other harmful components. purplish does not make any warranties or representations regarding the use of the materials in this site in terms of their correctness, accuracy, adequacy, usefulness, timeliness, reliability or otherwise. the above limitations may not apply to you. 
            </p>
            
        </div>
    </div>
</div>

<?php include(INCLUDES. 'footer.php'); ?>