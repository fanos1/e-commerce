
<?php 
require('../includes/config.inc.php');

$page_title = 'About Dobaln Fashion Ltd';
include(INCLUDES.'header.php'); 
?>


<div class="container" style="padding:2em;">
    <div class="row">
        <div class="col-lg-12">
            <h1>About Us</h1>
            <p>
                Dobaln is ladies online fashion shop which serves the UK market. We specialise in printed knitted ladies wear and clothing. Our business is located in North London.
            </p>
            <h2>Our mission</h2>
            <p>
                We strive to offer unique fashionable woman's clothing which is trendy.
            </p>
        </div>
    </div>
</div>

<?php include(INCLUDES. 'footer.php'); ?>