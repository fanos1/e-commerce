
<?php 
require('../includes/config.inc.php');

include(INCLUDES.'header.php'); 
?>


<div class="container" style="padding:2em;">
    <div class="row">
        <div class="col-lg-12">
            <h2>Shipping</h2>
            <p>We shipp only within the UK. Shipping is FREE</p>
            
            <h3>Returns</h3>
            <p>
             Faulty products can be returned to us within 10 days of delivery. If you don't like the product you purchased, you can return it to
             us within 10 days and we will refund you the money as long as the product has not been used.
            </p>
            
            <p>
                Please call us back as soon as possible if you would like the product to be returned. You will need to pay the postage fees yourself.
            </p>
            
        </div>
    </div>
</div>

<?php include(INCLUDES. 'footer.php'); ?>