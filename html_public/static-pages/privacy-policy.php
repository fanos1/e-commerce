
<?php 
require('../includes/config.inc.php');

include(INCLUDES.'header.php'); 
?>


<div class="container" style="padding:2em;">
    <div class="row">
        <div class="col-lg-12">
            <h1>Our Privacty Policy</h1>
            <p>
                We don't store any credit card information in our databases. We only collect our customer's name, address and phone details. We collect this in order 
                to complete the order properly. We will never share this information with any other 3rd party. 
                We comply with the (Data Protection Act 1998) standards, procedures and requirements laid down in the Act to ensure that the personal information you give us is kept secure and processed fairly and lawfully.
            </p>
            <p>                
                Information is never used for individual company purposes. Your information will not be shared with third parties regarding marketing and research without your individual agreement.
            </p>
        </div>
    </div>
</div>

<?php include(INCLUDES. 'footer.php'); ?>