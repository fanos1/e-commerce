
<?php 
require('../includes/config.inc.php');

$page_title = 'About Dobaln Fashion Ltd';
include(INCLUDES.'header.php');

//EVENT CALENDAR FOR FASHION TAKEN FROM :: http://www.londonfashionweek.co.uk/Schedules
?>


<div class="container" style="padding:2em;">
    <div class="row">
        
        <div class="col-lg-12">
            <h1>London Fashion Events</h1>
            <h3>
               Date: Thursday 18th February
            </h3>            
            <p>
                International Fashion Showcase 2016 Launch Party. This event will be at Somerset House. Designers from 
                different countries are coming to London. Do not miss out!!
            </p>
        </div>
        
    </div>
    
    
    <div class="row">
        
        <div class="col-lg-12">            
            <h3>
               Date: 19th February 2016 
            </h3>            
            <p>
                The annual International Fashion Showcase presents 80 emerging designers from 24 countries. Open to the public. 
                <br/>
                Location: West Wing Galleries, Somerset House, Strand, London WC2R 1LA 
            </p>
        </div>
        
    </div>
</div>

<?php include(INCLUDES. 'footer.php'); ?>