<?php

class Customer {
	
	
        public function __construct() {

        }
	
	
	/*
	*
	*/
	public static function add_customer($dbc, $e, $f, $l, $a1, $a2, $c, $s, $z, $p )  {
            
            $customerLastInsertID = 0; //FALSE
            
            try {
                
                $q = "
		INSERT INTO customers (email, first_name, last_name, address1, address2, city, state, zip, phone) 
		VALUES (:e, :f, :l, :a1, :a2, :c, :s, :z, :p);
		";
		
                $stmt = $dbc->prepare($q); 
                
                $stmt->bindParam(':e', $e);											
                $stmt->bindParam(':f', $f);
                $stmt->bindParam(':l', $l);
                $stmt->bindParam(':a1', $a1);
                $stmt->bindParam(':a2', $a2);
                $stmt->bindParam(':c', $c);
                $stmt->bindParam(':s', $s);
                $stmt->bindParam(':z', $z);
                $stmt->bindParam(':p', $p);
                
                //PDOStatement->execute() returns true on success for INSERT
                if( $stmt->execute() ) {
                    
                    //IF INSERT was successfull, Retrieve the customer ID:
                    $customerLastInsertID = $dbc->lastInsertId();
                    
                    return $customerLastInsertID;
                } 
                
		//SELECT LAST_INSERT_ID() INTO cid;
                
                /*
                    $stmt = $db->prepare("...");
                    $stmt->execute();
                    $id = $db->lastInsertId();
                 */
                
            } catch (Exception $ex) {

            }
		
	}
	
	
	
} //End Cart