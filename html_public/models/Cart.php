<?php

class Cart {
	
	
    public function __construct() {
        
    }
	
	
	public static function update_cart($dbc, $uid, $sp_type, $pid, $qty) { 
	
            $result = FALSE;
            
            try {    
					
                if($qty > 0) {
                    $q = "
                    UPDATE carts 
                    SET quantity = :qty, date_modified = NOW() 
                    WHERE user_session_id = :uid 
                    AND product_type = :type 
                    AND product_id = :pid
                    ";	
                    
                    $stmt = $dbc->prepare($q);   
                    //bind $uid
                    $stmt->bindParam(':qty', $qty);
                    $stmt->bindParam(':uid', $uid);
                    $stmt->bindParam(':type', $sp_type);
                    $stmt->bindParam(':pid', $pid);

                    $r = $stmt->execute();	
                    
                    if($r) {
                        $result = TRUE;
                    }

                } else if($qty == 0) {
                    //Calling a static method() from another method() we call it with self::
                    self::remove_from_cart($dbc, $uid, $sp_type, $pid); 
                }			

                return $result;                        				

            } catch (PDOException $e) {            

            }		
		
	}
	

       
        
	public static function move_to_cart($dbc, $uid, $sp_type, $pid, $qty) { 
		                
            try {
                    $q1 = "
                    SELECT id FROM carts 
                    WHERE user_session_id = :uid 
                    AND product_type = :type 
                    AND product_id = :pid
                    ";

                    $stmt = $dbc->prepare($q1);            

                    //bind $uid
                    $stmt->bindParam(':uid', $uid);
                    $stmt->bindParam(':type', $sp_type);
                    $stmt->bindParam(':pid', $pid);

                    $stmt->execute();
                    $r1 = $stmt->fetch(PDO::FETCH_ASSOC);

                    $cid = $r1['id'];                        
                    
                    /*
                    This procedure will be called when the customer clicks a link. 
                    In that case, a simple INSERT is all that’s necessary. 
                    However, if the user clicks the same link again later—thereby adding to the cart something already in the cart, 
                    another INSERT should NOT be executed. The decision I made in the book is to assume that 
                    the customer purposefully wanted to add another quantity of the same 
                    item to the cart in such instances. So the stored procedure runs an UPDATE query when the item exists in the cart
                    */
                    
                    if($cid > 0) { // it will be 1 IF current user HAS ALREADY added this  item before into table.
                        
                        //UPDATE cart for this item if this user has already added this item to his cart. Increment by 1				
                        
                        $q2 = "
                        UPDATE carts 
                        SET quantity = quantity + 1, date_modified = NOW() 
                        WHERE id = :cid
                        ";				
                        $stmt = $dbc->prepare($q2); 
                        $stmt->bindParam(':cid', $cid);
                        $r2 = $stmt->execute();			

                        if(!$r2) {
                            exit ("<h1>INSERT QUERy failed, satr 118, Cart Class </h1>");                            
                        }
                        return $r2;
                        

                    } else { // IF this use has not alread added this item in Cart, INSERT! 
                        
                        $q2 = "
                        INSERT INTO carts (user_session_id, product_type, product_id, quantity) 
                        VALUES (:uid, :type, :pid, :qty)
                        ";
                        $stmt = $dbc->prepare($q2); 
                        $stmt->bindParam(':uid', $uid);
                        $stmt->bindParam(':type', $sp_type);				
                        $stmt->bindParam(':pid', $pid);								
                        $stmt->bindParam(':pid', $qty);	

                        $r2 = $stmt->execute();

                        if(!$r2) {
                            exit( "<h1>INSERT QUERy failed, satr 135, Cart Class </h1>");                        
                        }				
                        
                        return  $r2;
                    }		
                    
                    

            } catch (PDOException $e) {            

            }		
		
	}
        
        
	
	public static function add_to_cart($dbc, $uid, $sp_type, $pid, $qty, $size) { 
		            
            $result = false;
                
            try {    
                    $q1 = "
                    SELECT id FROM carts 
                    WHERE user_session_id = :uid 
                    AND product_type = :type 
                    AND product_id = :pid
                    ";

                    $stmt = $dbc->prepare($q1);
					
                        if($stmt) {
                            $stmt->bindParam(':uid', $uid);
                            $stmt->bindParam(':type', $sp_type);
                            $stmt->bindParam(':pid', $pid);

                                $r1 = $stmt->execute();						
                                if($r1) {							
                                    $rows = $stmt->fetchAll();
                                }
                        }

                        $howManyRows = count($rows);

                        //if($cid) {
                        if($howManyRows > 0) { //it means this user has already added item to cart, UPDATE
						
                            $cid = $rows[0]['id'];                        
                                                
                            //UPDATE cart for this item if this user has already added this item to his cart. Increment by 1				                        
                            $q2 = "
                            UPDATE carts 
                            SET quantity = quantity + 1, date_modified = NOW() 
                            WHERE id = :cid
                            ";				
                            $stmt = $dbc->prepare($q2); 
                            $stmt->bindParam(':cid', $cid);
                            $r2 = $stmt->execute();			

                            if(!$r2) {
                                exit("Error with Cart, str184");
                            } else {
                                //return TRUE; //UPDATE query successfull
                                $result = TRUE;
                            }						
						
                        } else {
					
                            $q2 = "
                            INSERT INTO carts (user_session_id, product_type, product_id, quantity, size) 
                            VALUES (:uid, :type, :pid, :qty, :size)
                            ";
                            $stmt = $dbc->prepare($q2); 

                            $stmt->bindParam(':uid', $uid);
                            $stmt->bindParam(':type', $sp_type);				
                            $stmt->bindParam(':pid', $pid);								
                            $stmt->bindParam(':qty', $qty);	
                            $stmt->bindParam(':size', $size);

                            $r2 = $stmt->execute();

                            if(!$r2) {
                                exit("Error with Cart, str204");
                            } else {
                                //return TRUE; //INSERT query successfull
                                $result = TRUE;
                            }
                        
                        }
			
                        return $result;

            } catch (PDOException $e) {            
                echo "<h3>Error! we apologize. str217 </h3>";
                //echo $e->getMessage();
                //echo $e->getMessage();
            }		
		
	}
	
	
	
	/*
	* CALL remove_from_cart('$uid', '$type', $pid)")
	*/
	public static function remove_from_cart($dbc, $uid, $sp_type, $pid) {
		
            try {    					
                $q = "
                DELETE FROM carts 
                WHERE user_session_id = :uid 
                AND product_type = :type 
                AND product_id = :pid
                ";

                $stmt = $dbc->prepare($q); 
                $stmt->bindParam(':uid', $uid);
                $stmt->bindParam(':type', $sp_type);				
                $stmt->bindParam(':pid', $pid);								
                $r = $stmt->execute();

                if(!$r) {                    
                    return FALSE; //could no remove
                } else {                    
                    return TRUE; 
                }
			
            } catch (PDOException $e) {            
                     
            }	
		
	}
	
        
        /*
         * Delete everyting from cart. This method is called when user has paid for item successfully         
         */
        public static function clearCart($dbc, $uid) {
            
            try {
                
                //DELETE FROM carts WHERE user_session_id=uid;
                $q = " DELETE FROM carts WHERE user_session_id=:uid ";

                $stmt = $dbc->prepare($q); 
                $stmt->bindParam(':uid', $uid);								
                $r = $stmt->execute();

                if(!$r) {                    
                    return FALSE; //could no remove
                } else {                    
                    return TRUE; 
                }
                
            } catch (Exception $ex) {

            }
            
        }
	
	
	/*
	*
	*/
	public static function get_shopping_cart_contents($dbc, $uid) {
		
            try {    					
                $q = "
                SELECT CONCAT('G', ncp.id) AS sku, 
                c.quantity, 
                ncc.category,
                c.size,
                ncp.name, 
                ncp.price, 
                ncp.stock, 
                ncp.image,
                sales.price AS sale_price 
                FROM carts AS c 
                INNER JOIN products AS ncp ON c.product_id = ncp.id 
                INNER JOIN categories AS ncc ON ncc.id = ncp.non_coffee_category_id 
                LEFT OUTER JOIN sales 
                ON (sales.product_id = ncp.id 
                        AND sales.product_type='goodies' 
                        AND ((NOW() BETWEEN sales.start_date AND sales.end_date) 
                        OR (NOW() > sales.start_date AND sales.end_date IS NULL)) 
                ) 
                WHERE c.product_type='goodies' 
                AND c.user_session_id = :uid 

                ";

                $stmt = $dbc->prepare($q);            						
                $stmt->bindParam(':uid', $uid);			
                $stmt->execute();
                //$r = $stmt->fetch(PDO::FETCH_ASSOC	);
                $r = $stmt->fetchAll();
                
                /*
                sku 	quantity 	category    size	name 	price 	stock 	sale_price   image 	
                G1 	4               Cardigans   s/m         item1 	650 	100 	500          xxx.jpg       
                */
               
                return $r;                        				
			
            } catch (PDOException $e) {            
                echo "<h3>Error occured while retrieveing shopping cart items. We apoligize!</h3>";
                echo $e->getMessage(); //debug only
            }	
		
	}
	
	
        
	/*
	* ======================= WISH LIST ====================
	*/
	public static function update_wish_list($dbc, $uid, $sp_type, $pid, $qty) {
		
		try {
			
                    if($qty > 0) {

                        $q = "
                        UPDATE wish_lists 
                        SET quantity = :qty, date_modified = NOW() 
                        WHERE user_session_id = :uid 
                        AND product_type = :type 
                        AND product_id = :pid
                        ";
                        $stmt = $dbc->prepare($q); 
                        $stmt->bindParam(':qty', $qty);
                        $stmt->bindParam(':uid', $uid);
                        $stmt->bindParam(':type', $sp_type);				
                        $stmt->bindParam(':pid', $pid);								
                        $r = $stmt->execute();

                    } else {
                        $r = self::remove_from_wish_list ($dbc, $uid, $sp_type, $pid);
                        if($r) {
                            echo "<h3>item REMOVED from WICH LIST. str343</h3>";
                        } else {
                            echo "<h3>item COULD NOT be removed str345</h3>";
                        }
                    }
				
		} catch (PDOException $e) {
			
		}
		
	}
	
	/*
	*
	*/
	public static function add_to_wish_list($dbc, $uid, $sp_type, $pid, $qty) {
		
                $result = FALSE;
                
		try {                    
			$q = "
			SELECT id 
			FROM wish_lists 
			WHERE user_session_id = :uid 
			AND product_type = :type 
			AND product_id = :pid;
			";
			$stmt = $dbc->prepare($q); 
                        
                        if($stmt) {
                            
                            $stmt->bindParam(':uid', $uid);
                            $stmt->bindParam(':type', $sp_type);
                            $stmt->bindParam(':pid', $pid);

                            $stmt->execute();
                        }
                        
			
			$r = $stmt->fetch(PDO::FETCH_ASSOC);
			
			$cid = $r['id'];
			
			if($cid > 0) {
				$q1 = "
				UPDATE wish_lists 
				SET quantity = quantity + :qty, date_modified=NOW() 
				WHERE id = :cid
				";
				$stmt = $dbc->prepare($q1); 
				$stmt->bindParam(':qty', $qty);
				$stmt->bindParam(':cid', $cid);	
				//$r1 = $stmt->execute();                                
				//return $r1;
                                
                                $result = $stmt->execute() ? TRUE : FALSE;
                                
			} else {				
				$q1 = "
				INSERT INTO wish_lists (user_session_id, product_type, product_id, quantity) 
				VALUES (:uid, :type, :pid, :qty)			
				";
				$stmt = $dbc->prepare($q1); 
				$stmt->bindParam(':uid', $uid);
				$stmt->bindParam(':type', $sp_type);	
				$stmt->bindParam(':pid', $pid);				
				$stmt->bindParam(':qty', $qty);							
				//$r1 = $stmt->execute();
				//return $r1;			
                                
                                $result = $stmt->execute() ? TRUE : FALSE;
			}
                        return $result;
			
		} catch(PDOException $e) {
                    echo "<h2>Error with adding to wish list, we apologize!</h2>";
                    echo $e->getMessage();                    
		}
				
	}
	
	
	/*
	*/
	public static function remove_from_wish_list($dbc, $uid,  $sp_type, $pid) {
		
                $result = FALSE;
                
		try {		
			$q = "
			DELETE FROM wish_lists 
			WHERE user_session_id = :uid 
			AND product_type = :type 
			AND product_id = :pid
			";	
			
			$stmt = $dbc->prepare($q); 
			$stmt->bindParam(':uid', $uid);						
			$stmt->bindParam(':type', $sp_type);				
			$stmt->bindParam(':pid', $pid);								
			$r = $stmt->execute();
			
			if($r) {
                            $result = true;
                        }
                        
                        return $result;
			
		} catch(PDOException $e) {
			
		}   
		
	}
	
	/*
	*/
	public static function get_wish_list_contents($dbc, $uid) {
            
		try {
			$q = "
			SELECT CONCAT('G', ncp.id) AS sku, 
			wl.quantity, ncc.category, ncp.name, ncp.price, ncp.stock, 
			sales.price AS sale_price 
			FROM wish_lists AS wl 
			INNER JOIN products AS ncp ON wl.product_id = ncp.id 
			INNER JOIN categories AS ncc ON ncc.id=ncp.non_coffee_category_id 
			LEFT OUTER JOIN sales ON (
				sales.product_id = ncp.id 
				AND sales.product_type = 'goodies' 
				AND ((NOW() BETWEEN sales.start_date 
				AND sales.end_date) 
				OR (NOW() > sales.start_date AND sales.end_date IS NULL)) 
			) 
				WHERE wl.product_type='goodies' AND wl.user_session_id = :uid 
			UNION 
			SELECT CONCAT('C', sc.id), wl.quantity, gc.category, 
			CONCAT_WS(' - ', s.size, sc.caf_decaf, sc.ground_whole), sc.price, sc.stock, sales.price 
			FROM wish_lists AS wl 
			INNER JOIN specific_coffees AS sc ON wl.product_id=sc.id 
			INNER JOIN sizes AS s ON s.id=sc.size_id 
			INNER JOIN general_coffees AS gc ON gc.id=sc.general_coffee_id 
			LEFT OUTER JOIN sales 
			ON (sales.product_id=sc.id 
				AND sales.product_type='coffee' 
				AND ((NOW() BETWEEN sales.start_date 
				AND sales.end_date) 
				OR (NOW() > sales.start_date AND sales.end_date IS NULL)) 
			) 
				WHERE wl.product_type='coffee' AND wl.user_session_id = :uid
			";
			$stmt = $dbc->prepare($q); 
			$stmt->bindParam(':uid', $uid);											
			$stmt->execute();
			
			$r = $stmt->fetchAll();
			return $r;
			
		} catch (PDOException $e) {
			
		}
	}
		
	
} //End Cart