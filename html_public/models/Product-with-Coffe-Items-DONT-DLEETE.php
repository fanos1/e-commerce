<?php

class Products {

    public $id = null;

    public function __construct() {
        
    }

    /* Select Categories */

    public static function select_categories($dbc, $sp_type) {

        try {

            if ($sp_type == 'coffee') {
                $q = "SELECT * FROM general_coffees ORDER by category";
            } else {
                $q = "SELECT * FROM categories ORDER by category";
            }
            $stmt = $dbc->query($q);
            $r = $stmt->fetchAll();
            return $r;
        } catch (PDOException $e) {
            
        }
    }

    /* Select Products */

    public static function select_products($dbc, $sp_type, $sp_cat) {

        try {

            if ($sp_type == 'coffee') {
                $q = "
                    SELECT 
                    gc.description, 
                    gc.image, 
                    CONCAT('C', sc.id) AS sku, 
                    CONCAT_WS(' - ', s.size, sc.caf_decaf, sc.ground_whole, 
                    CONCAT('£', FORMAT(sc.price/100, 2))) AS name, 
                    sc.stock, sc.price, sales.price AS sale_price 
                    FROM specific_coffees AS sc 
                    INNER JOIN sizes AS s ON s.id = sc.size_id 
                    INNER JOIN general_coffees AS gc ON gc.id = sc.general_coffee_id 
                    LEFT OUTER JOIN sales ON (sales.product_id = sc.id 
                            AND sales.product_type = 'coffee' 
                            AND ((NOW() BETWEEN sales.start_date AND sales.end_date) 
                            OR (NOW() > sales.start_date AND sales.end_date IS NULL)) 
                    ) 
                    WHERE general_coffee_id = $sp_cat AND stock > 0 
                    ORDER by name
                    ";
                
            } else {
                $q = "
                    SELECT ncc.description AS g_description, 
                    ncc.image AS g_image, 
                    CONCAT('G', ncp.id) AS sku, 
                    ncp.name, 
                    ncp.description, 
                    ncp.image, 
                    ncp.price, 
                    ncp.stock, 
                    s.size,
                    sales.price AS sale_price
                    FROM products AS ncp 
                    INNER JOIN sizes AS s ON s.id = ncp.size
                    INNER JOIN categories AS ncc 
                    ON ncc.id = ncp.non_coffee_category_id 
                    LEFT OUTER JOIN sales ON (sales.product_id = ncp.id 
                            AND sales.product_type='goodies' 
                            AND ((NOW() BETWEEN sales.start_date AND sales.end_date) 
                            OR (NOW() > sales.start_date AND sales.end_date IS NULL)) 
                    )
                    WHERE non_coffee_category_id = $sp_cat 
                    ORDER by date_created DESC
                    ";
            }

            $stmt = $dbc->query($q);
            $r = $stmt->fetchAll();           
            return $r;
            
        } catch (PDOException $e) {
            
        }
    }
    
    /* 
     * qUERY for specific item details
     * this query is similar to select_product() method above, but will also display size options
     */
    public static function select_product_byID($dbc, $itemId)  {
      
        $q = "
            SELECT ncp.id, 
            ncp.non_coffee_category_id, 
            ncp.name, ncp.description, 
            ncp.image, ncp.price, 
            ncp.stock, 
            s.size, 
            ncp.date_created,
            ncp.product_code,
            CONCAT('G', ncp.id) AS sku,
            sales.price AS sale_price
            FROM products AS ncp
            INNER JOIN sizes AS s ON s.id = ncp.size
            LEFT OUTER JOIN sales ON (sales.product_id = ncp.id 
                    AND sales.product_type='goodies' 
                    AND ((NOW() BETWEEN sales.start_date AND sales.end_date) 
                    OR (NOW() > sales.start_date AND sales.end_date IS NULL)) 
                )
            WHERE ncp.id = $itemId            
            ";    
        
        //for testing RETURN item with ID  = 1
        
        $stmt = $dbc->query($q);
        $r = $stmt->fetchAll();
        return $r;

    }

    

    /* Select Products */

    public static function select_sale_items($dbc, $get_all = true) { //Default is to get All sale items
        try {

            if ($get_all) { //if TRUE
                $q = "
                SELECT CONCAT('G', ncp.id) AS sku, 
                    sa.price AS sale_price, 
                    ncc.category, 
                    ncp.image, 
                    ncp.name, 
                    ncp.price AS price, 
                    ncp.stock, 
                    ncp.description 
                    FROM sales AS sa 
                    INNER JOIN products AS ncp ON sa.product_id = ncp.id 
                    INNER JOIN categories AS ncc ON ncc.id = ncp.non_coffee_category_id 
                    WHERE sa.product_type = 'goodies' 
                            AND ((NOW() BETWEEN sa.start_date AND sa.end_date) 
                            OR (NOW() > sa.start_date AND sa.end_date IS NULL) 
                    )
                    UNION 
                    SELECT CONCAT('C', sc.id), sa.price, gc.category, gc.image, 
                    CONCAT_WS(' - ', s.size, sc.caf_decaf, sc.ground_whole), sc.price, sc.stock, gc.description 
                    FROM sales AS sa 
                    INNER JOIN specific_coffees AS sc ON sa.product_id = sc.id 
                    INNER JOIN sizes AS s ON s.id=sc.size_id 
                    INNER JOIN general_coffees AS gc ON gc.id = sc.general_coffee_id 
                    WHERE sa.product_type = 'coffee' 
                            AND ((NOW() BETWEEN sa.start_date AND sa.end_date) 
                            OR (NOW() > sa.start_date AND sa.end_date IS NULL) 
                    )
                ";
            } else {
                $q = "
                (    
                SELECT CONCAT('G', ncp.id) AS sku, 
                CONCAT('£', FORMAT(sa.price/100, 2)) AS sale_price, 
                    ncc.category, 
                    ncp.image, 
                    ncp.name 
                    FROM sales AS sa 
                    INNER JOIN products AS ncp ON sa.product_id=ncp.id 
                    INNER JOIN categories AS ncc ON ncc.id=ncp.non_coffee_category_id 
                    WHERE sa.product_type='goodies' 
                            AND ((NOW() BETWEEN sa.start_date 
                            AND sa.end_date) OR (NOW() > sa.start_date AND sa.end_date IS NULL) ) 
                    ORDER BY RAND() LIMIT 2
                    ) 
                    UNION 
                    (
                    SELECT CONCAT('C', sc.id), 
                    CONCAT('£', FORMAT(sa.price/100, 2)), gc.category, gc.image, 
                    CONCAT_WS(' - ', s.size, sc.caf_decaf, sc.ground_whole) 
                    FROM sales AS sa 
                    INNER JOIN specific_coffees AS sc ON sa.product_id = sc.id 
                    INNER JOIN sizes AS s ON s.id = sc.size_id 
                    INNER JOIN general_coffees AS gc ON gc.id=sc.general_coffee_id 
                    WHERE sa.product_type = 'coffee' 
                            AND ((NOW() BETWEEN sa.start_date 
                            AND sa.end_date) OR (NOW() > sa.start_date AND sa.end_date IS NULL) ) 
                    ORDER BY RAND() LIMIT 2
                    )
                ";
            }

            $stmt = $dbc->query($q);
            $r = $stmt->fetchAll();
            return $r;
        } catch (PDOException $e) {
            
        }
    }

}