<?php

class Order {
	
	
        public function __construct() {

        }
	
	
	/*
	 * INSERT an order INTO the ORDER table
         * Return the last inserted orderID
	*/
	public static function add_order($dbc, $cid, $uid, $ship, $cc )  {
                        
            try {
                
                //We want 2 values returned: $order_total and $order_id
                $valueToReturn = array(); 
                
                $dbc->beginTransaction();
                
                //1st QUERY
                $q = "
                    INSERT INTO orders (customer_id, shipping, credit_card_number, order_date) 
                        VALUES ( :cid, :ship, :cc, NOW() )";
		
                $stmt = $dbc->prepare($q);                 
                $stmt->bindParam(':cid', $cid);
                $stmt->bindParam(':ship', $ship);
                $stmt->bindParam(':cc', $cc);
                $stmt->execute();
                
                $lastOrderId = $dbc->lastInsertId();
                
                //2ND QUERY
                $q = "
                    SELECT 
                    c.product_type, 
                    c.product_id, 
                    c.quantity, 
                    IFNULL(sales.price, ncp.price) AS price_per
                    FROM carts AS c 
                    INNER JOIN products AS ncp ON c.product_id=ncp.id 
                    LEFT OUTER JOIN sales ON (
                            sales.product_id=ncp.id AND sales.product_type='goodies' 
                            AND ((NOW() BETWEEN sales.start_date AND sales.end_date) 
                            OR (NOW() > sales.start_date AND sales.end_date IS NULL)) 
                    ) 
                    WHERE c.product_type='goodies' AND c.user_session_id = :uid
                ";
                $stmt = $dbc->prepare($q);                 
                $stmt->bindParam(':uid', $uid);
                $stmt->execute();
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                
                
                $x1 = $result['product_type'];  //goodies
                $x2 = $result['product_id'];    //17                    
                $x3 = $result['quantity'];      //1
                $x4 = $result['price_per'];     //2500
                
                
                $res = $dbc->exec("INSERT INTO order_contents 
                        (order_id, product_type, product_id, quantity, price_per)
                            VALUES($lastOrderId, '$x1', $x2, $x3, $x4 )");
                
                
                //PDO::exec() Returns the number of rows that were modified or deleted by the SQL statement 
                //echo "<h1>RES:: $res</h1>"; //OUT::: 1
                
                
                //4th QUERY
                $stmt2 = $dbc->query("SELECT SUM(quantity*price_per) AS subtotal 
                        FROM order_contents WHERE order_id=$lastOrderId");
                $y = $stmt2->fetch(PDO::FETCH_ASSOC);
                //$subtotal = $y['subtotal'];
                
                $total = $y['subtotal'] + $ship;
                
                
                //QUERY                
                $dbc->exec("UPDATE orders SET total = $total  WHERE id=$lastOrderId");
                /*  
                if( $dbc->exec("UPDATE orders SET total = $total  WHERE id=$lastOrderId") ) {                  
                    echo "<h1>All good</h1>";
                } 
                 * 
                 */
                    
                
                if( $dbc->commit() ) {
                    array_push($valueToReturn, $lastOrderId);
                    array_push($valueToReturn, $total);
                }
                
                return $valueToReturn;
                
            } catch (Exception $ex) {
                 //echo 'Exception:: 78';
                 //echo $ex->getMessage(); //DEBUG
                 $dbc->rollBack();
            }
		
	}
	
        
        public static function get_order_contents($dbc, $oid) {
            
            try {
                $q = "SELECT oc.quantity, oc.price_per, (oc.quantity*oc.price_per) AS subtotal, 
                    ncc.category, ncp.name, o.total, o.shipping 
                    FROM order_contents AS oc 
                    INNER JOIN products AS ncp ON oc.product_id=ncp.id 
                    INNER JOIN categories AS ncc ON ncc.id=ncp.non_coffee_category_id 
                    INNER JOIN orders AS o ON oc.order_id=o.id 
                    WHERE oc.product_type='goodies' AND oc.order_id = :oid 
                ";
                $stmt = $dbc->prepare($q);                 
                $stmt->bindParam(':oid', $oid);
                $stmt->execute();
                $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
                
                return $r;                
                
            } catch (Exception $ex) {

            }
        }


        /*
         * INSERTs INOT order_content TABLE the SLELECT subquery
         */
        public static function add_charge($dbc, $chargeId, $orderIdLast, $transacType, $orderTotal, $fullResponse )  {
            
            try {
                
                $q = "INSERT INTO charges VALUES (NULL, :charge_id, :oid, :trans_type, :orderTotal, :fullResponse, NOW())";
                $stmt = $dbc->prepare($q); 
                
                
                $stmt->bindParam(':charge_id', $chargeId);
                $stmt->bindParam(':oid', $orderIdLast);
                $stmt->bindParam(':trans_type', $transacType);
                $stmt->bindParam(':orderTotal', $orderTotal);
                $stmt->bindParam(':fullResponse', $fullResponse);
                
                return $stmt->execute();
                
                
            } catch (Exception $ex) {
                echo "<h3>Exception Error occured, we apoligize str155</h3>";
                //echo $ex->getMessage();
                
            }
            
        }
	
	
} //End Cart