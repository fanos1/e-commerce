<?php
require('./includes/config.inc.php');
include('./includes/product_functions.inc.php');


// Check for the user's session ID, to retrieve the cart contents:
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    
    if (isset($_COOKIE['SESSION']) && (strlen($_COOKIE['SESSION']) === 32)) {
                
            $uid = $_COOKIE['SESSION'];
            
            // Use the existing user ID:
            session_id($uid);
            // Start the session:
            session_start();
            
    } else { // Redirect the user.
            //$location = 'http://' . BASE_URL . 'cart.php';
            $location = 'https://' . BASE_URL . 'cart.php';
            header("Location: $location");
            exit();
    }

} else { // POST request.
    session_start();
    $uid = session_id();
}



//require(MYSQL);
require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise</h3>");
}
include(MODELS. 'Cart.php');
include(MODELS. 'Customer.php');


$shipping_errors = array();

// Check for a form submission:
if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
	// Check for Magic Quotes:
	if (get_magic_quotes_gpc()) {
            $_POST['first_name'] = stripslashes($_POST['first_name']);
            // Repeat for other variables that could be affected.
	}

	// Check for a first name:
	if (preg_match ('/^[A-Z \'.-]{2,20}$/i', $_POST['first_name'])) {
            $fn = addslashes($_POST['first_name']);
	} else {
            $shipping_errors['first_name'] = 'Please enter your first name!';
	}
	
	// Check for a last name:
	if (preg_match ('/^[A-Z \'.-]{2,40}$/i', $_POST['last_name'])) {
            $ln  = addslashes($_POST['last_name']);
	} else {
            $shipping_errors['last_name'] = 'Please enter your last name!';
	}
	
	// Check for a street address:
	if (preg_match ('/^[A-Z0-9 \',.#-]{2,80}$/i', $_POST['address1'])) {
		$a1  = addslashes($_POST['address1']);
	} else {
		$shipping_errors['address1'] = 'Please enter your street address!';
	}

	// Check for a second street address:
	if (empty($_POST['address2'])) {
		$a2 = NULL;
	} elseif (preg_match ('/^[A-Z0-9 \',.#-]{2,80}$/i', $_POST['address2'])) {
		$a2 = addslashes($_POST['address2']);
	} else {
		$shipping_errors['address2'] = 'Please enter your street address!';
	}
	
	// Check for a city:
	if (preg_match ('/^[A-Z \'.-]{2,60}$/i', $_POST['city'])) {
		$c = addslashes($_POST['city']);
	} else {
		$shipping_errors['city'] = 'Please enter your city!';
	}
	
        /* 
	// Check for a state:
	if (preg_match ('/^[A-Z]{2}$/', $_POST['state'])) {
            $s = $_POST['state'];
	} else {
            $shipping_errors['state'] = 'Please enter your state!';
	}
         * 
         */
        $s = 'AL'; //later remove $s and adjust 'customer' table in DATABASE, we don't need state for this website
	
        
	//if (preg_match ('/^(\d{5}$)|(^\d{5}-\d{4})$/', $_POST['zip'])) {
        if( preg_match('/^(?:[A-Za-z]\d ?\d[A-Za-z]{2})|(?:[A-Za-z][A-Za-z\d]\d ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d{2} ?\d[A-Za-z]{2})|(?:[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d[A-Za-z] ?\d[A-Za-z]{2})$/', $_POST['zip'] ) ) 
        {
            $z = $_POST['zip'];            
            //echo 'TRUE postcode';
            
        } else {
            $shipping_errors['zip'] = 'Please enter your post code!';
        }                
        
	
	// Check for a phone number:
	// Strip out spaces, hyphens, and parentheses:
	$phone = str_replace(array(' ', '-', '(', ')'), '', $_POST['phone']);
	if (preg_match ('/^[0-9]{11,14}$/', $phone) ) {
            $p  = $phone;
	} else {
            $shipping_errors['phone'] = 'Please enter your phone number!';
	}
	
	// Check for an email address:
	if ( filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
            $e = $_POST['email'];
            $_SESSION['email'] = $_POST['email'];
	} else {
            $shipping_errors['email'] = 'Please enter a valid email address!';
	}
	
	// Check if the shipping address is the billing address:
        // We stored these in SESSION so that we can re-use them in billing.php page. This saves users inputting same FORM values again
	if (isset($_POST['use']) && ($_POST['use'] === 'Y')) {            
		$_SESSION['shipping_for_billing'] = true; //@ billing.php page, we check to see if SESSION[shipping_for_billing] is TRUE. if true,                  
		$_SESSION['cc_first_name']  = $_POST['first_name'];
		$_SESSION['cc_last_name']  = $_POST['last_name'];
		$_SESSION['cc_address']  = $_POST['address1'] . ' ' . $_POST['address2'];
		$_SESSION['cc_city'] = $_POST['city'];
		//$_SESSION['cc_state'] = $_POST['state'];
		$_SESSION['cc_zip'] = $_POST['zip'];
	}

        
        // -------------------------
        // If everything's OK...
        //---------------------------
	if (empty($shipping_errors))  
	{
		
            // Add the user to the database...            
            $customerLastInsertID = Customer::add_customer($dbc, $e, $fn, $ln, $a1, $a2, $c, $s, $z, $p);

            if($customerLastInsertID) {                             

                //list($_SESSION['customer_id']) = $customerLastInsertID; //THIS WAS RETURNING ONLY THE 1st DIGIT OF what the method() was returneing. i.e istead of 51, 5 was returning                
                $_SESSION['customer_id'] = $customerLastInsertID;                        
                //echo '<h3>CUSTOMER ID '. $_SESSION['customer_id'] .'</h3>';
                

                // Redirect to the next page:
                //$location = 'https://' . BASE_URL . 'billing.php';
                //$location = 'http://' . BASE_URL . 'billing.php';
                
                $location = 'https://' . BASE_URL . 'billing_stripe.php';
                header("Location: $location");
                exit();
                
            }


		// Log the error, send an email, panic!
		trigger_error('Your order could not be processed due to a system error. We apologize for the inconvenience.');

	} 

} // End of REQUEST_METHOD IF.

							


$rows = Cart::get_shopping_cart_contents($dbc, $uid);


//================= HTML ====================
//================= HTML ====================
$page_title = 'Dobaln Fashion - Checkout - Your Shipping Information';
//include('./includes/checkout_header.html');
include('./includes/header_checkout.php');

if($rows) {        
    //include ( VIEWS . "checkoutcart_view.php" );            
    include ( VIEWS . "checkout_view.php" ); 
    
} else {    
    include ( VIEWS . "emptycart_view.php" );   
}

// Finish the page:
include('./includes/footer.php');
?>