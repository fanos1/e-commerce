<style type="text/css">    
    /**
     * EasyZoom core styles
     */
    .easyzoom {
            position: relative;

            /* 'Shrink-wrap' the element */
            display: inline-block;
            *display: inline;
            *zoom: 1;
    }

    .easyzoom img {vertical-align: bottom;}
    .easyzoom.is-loading img {
            cursor: progress;
    }
    .easyzoom.is-ready img {
            cursor: crosshair;
    }
    .easyzoom.is-error  img {
            cursor: not-allowed;
    }

    .easyzoom-notice {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 150;
            width: 10em;
            margin: -1em 0 0 -5em;
            line-height: 2em;
            text-align: center;
            background: #FFF;
            box-shadow: 0 0 10px #888;
    }

    .easyzoom-flyout {
            position:absolute;
            z-index: 100;
            overflow: hidden;
            background: #FFF;
    }

    /**
     * EasyZoom layout variations
     */
    .easyzoom--overlay .easyzoom-flyout {
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
    }

    .easyzoom--adjacent .easyzoom-flyout {
            top: 0;
            left: 100%;
            width: 100%;
            height: 100%;
            margin-left: 20px;
    }
    
</style>
        

<?php 
/* 
if(isset($_GET['cat'], $_GET['catId']) ) {
    echo '
    <div class="container">    
        <div class="row">  
            <div class="col-lg-12" style="padding: 2em;">
                <p>
                    <a href="http://www.dobaln.co.uk/category/'.$_GET['cat'].'/'. $_GET['catId'].'" title="back">
                        <span class="glyphicon glyphicon-triangle-left"></span> Back 
                    </a>
                </p>            
            </div>
        </div>    
    </div>
    ';
    
}
 * 
 */

if(isset($_GET['cat'], $_GET['catId']) ) {
    echo '
    <div class="container">    
        <div class="row">  
            <div class="col-lg-12" style="padding: 2em;">                
                
                <ul class="breadcrumb">
                    <li><a href="/index.php">Home</a></li>
                    <li>
                        <a href="http://www.dobaln.co.uk/category/'.$_GET['cat'].'/'. $_GET['catId'].'" title="back">Back</a>
                    </li>
                </ul>
            </div>
        </div>    
    </div>
    ';
    
}

?>






      
    


<div class="container">	
    <div class="row">        
<?php

//ONLY 1 ITEM RETRUNED IS EXPECTED
foreach ($rows as $k => $array) {
    /* 
    echo '
    <h3>' . $array['name'] . '</h3>
        <div class="col-lg-6">
            <p>
                <img alt="' . $array['name'] . '" src="/products/' . $array['image']  . '" />' . 
                $array['description'] . '<br />' . get_price($type, $array['price'], $array['sale_price']) . 
                '<strong>Availability:</strong> ' . get_stock_status($array['stock']) . 
            '</p>
            <p><a href="/cart.php?sku=' . $array['sku'] . '&action=add" class="button">Add to Cart</a></p>                                       
        </div>';            
    
    echo '<h3>SIZE: '.$array['size'].'<h3>';
     * 
     */
    
    $pieces = explode("-", $array['size']);
    
?>    
        
    <div class="col-lg-6" style="overflow:hidden;">
        <div class="easyzoom easyzoom--overlay">            
            <a href="/products/<?php echo $array['image']; ?>" title="<?php echo $array['name']; ?>">
                <img src="/products/<?php echo $array['image']; ?>" alt="<?php echo $array['name']; ?>" width="280" />
            </a>
        </div>        
    </div>
        
    <div class="col-lg-6">      
            
            <h3><?php echo $array['name']; ?></h3>
            <p><?php 
                echo $array['description'] . '<br />' . 
                     '<strong>Product Code:</strong> '. $array['product_code'] . '<br />' .
                     get_price($type, $array['price'], $array['sale_price']) .
                     '<strong>Availability:</strong> ' . get_stock_status($array['stock'])
                ; 
                
                ?>
            </p>
            
            <form action="/cart.php" method="get">
                
                <input type="hidden" name="action" value="add" />                   
                <input type="hidden" name="sku" value="<?php echo $array['sku']; ?>" />   
                
                <select name="size">
                    <?php 
                    foreach ($pieces as $k => $v){
                        echo '<option value="'.trim($v).'">'. $v .'</option>';
                    }
                    ?>
                </select>
                <input type="submit" value="Add to Cart" class="button" />
                
            </form>
    </div> 
        
<?php } //FOREACH ?>

    
        
      
        
        
        
        
        
    </div><!-- row -->
</div>
    

<script src="/js/easyzoom.js"></script>
<script>
        // Instantiate EasyZoom plugin
        var $easyzoom = $('.easyzoom').easyZoom();

        // Get the instance API
        var api = $easyzoom.data('easyZoom');
</script>
