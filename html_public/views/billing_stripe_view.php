
<!-- ================== Display Carty ==================== -->
<div class="container">    
    <div class="row">        
        <div class="col-lg-12">            

        <h2>Your Shopping Cart</h2>
        <table class="table table-bordered">

            <tr>
                    <th align="center">image</th>
                    <th align="center">Item</th>
                    <th align="center">Quantity</th>
                    <th align="right">Price</th>
                    <th align="right">Subtotal</th>                    
            </tr>

            <?php

            $total = 0;

            // For removing problematic items:
            $remove = array();

            //Fetch each product
            foreach ($rows as $k => $array) {

                // Check the stock status:
                if ($array['stock'] < $array['quantity']) {

                    echo '
                    <tr class="text-danger">
                        <td colspan="4">
                            There are only ' . $array['stock'] . ' left in stock of the ' . $array['name'] . '. This item has been 
                                removed from your cart and placed in your wish list.
                        </td>
                    </tr>';

                    $remove[$array['sku']] = $array['quantity'];

                } else {

                    // Get the correct price:
                    $price = get_just_price($array['price'], $array['sale_price']);

                    // Calculate the subtotal:
                    $subtotal = $price * $array['quantity'];

                    // Print out a table row:
                    echo '
                    <tr>
                        <td>
                            <img src="/products/'.$array['image'].'" alt="'.$array['category'].'-'.$array['name'] .'" width="68" height="80" />
                        </td>
                        <td>' . $array['category'] . '-' . $array['name'] . '</td>
                        <td align="center">' . $array['quantity'] . '</td>
                        <td align="right">£' . $price . '</td>
                        <td align="right">£' . number_format($subtotal, 2) . '</td>
                    </tr>
                    ';

                    // Add the subtotal to the total:
                    $total += $subtotal;

                }
            }

            
            //$shipping = get_shipping($total);
            $shipping = 0; //FREE shipping
            $total += $shipping;
            echo '<tr>
                    <td colspan="2"> </td><th align="right">Shipping &amp; Handling</th>
                    <td align="right">£' . number_format($shipping, 2) . '</td>
            </tr>
            ';

            // Store the shipping in the session:
            $_SESSION['shipping'] = $shipping;

            // Display the total:
            echo '<tr>
                    <td colspan="2"> </td><th align="right">Total</th>
                    <td align="right">£' . number_format($total, 2) . '</td>
                    <td>&nbsp;</td>
            </tr>
            ';

            // Remove any problematic items:
            if (!empty($remove)) 
            {        
                // Clear the results:
                // ::: mysqli_next_result($dbc);



                // Loop through the array:
                foreach ($remove as $sku => $qty)  {

                    list($sp_type, $pid) = parse_sku($sku); //$remove Array stores [sku] as key

                    /* =================================================================
                    // Move it to the wish list:
                    $r = mysqli_multi_query(
                            $dbc, "CALL add_to_wish_list('$uid', '$sp_type', $pid, $qty);
                            CALL remove_from_cart('$uid', '$sp_type', $pid)"
                    );              
                    =============================================== 
                    */		


                    // Move it to the wish list:
                    $r = Cart::add_to_wish_list($dbc, $uid, $sp_type, $pid, $qty); //call
                    //if($r) { echo 'add to wish list successful'; }
                    

                    $r2 = Cart::remove_from_cart($dbc, $uid, $sp_type, $pid, $qty); //call                                
                    //if($r2) { echo 'remove from CART successful'; }

                }
            }

        ?>
        </table>

        </div>
    </div><!-- row -->
</div><!-- container -->
<!-- =============== END Display Carty ================ -->


<div class="container">    
    <div class="row">        
        <div class="col-lg-12">
            <?php if (isset($message)) echo "<p class=\"error\">$message</p>"; ?>
            
            <form action="/billing_stripe.php" method="POST" id="billing_form">
                <?php 
                    echo '<div class="alert alert-danger" id="error_span"></div>'; 
                    include('./includes/form_functions.inc.php');
                ?>
                <fieldset>

                    <div class="field">
                            <label for="cc_number"><strong>Card Number</strong></label><br />
                            <!-- <input type="text" id="cc_number" autocomplete="off"  value="6011111111111117" /> -->
                            <input type="text" id="cc_number" autocomplete="off" data-stripe="number" /><!-- Stripe will search for those attributes and fetch their respective values. -->
                    </div>

                    <div class="field">
                            <label for="exp_date"><strong>Expiration Date (MM/YYYY)</strong></label><br />
                            <!-- 
                            <input type="text" id="cc_exp_month" autocomplete="off" value="12" />/
                            <input type="text" id="cc_exp_year" autocomplete="off" value="2014"/> 
                            -->
                            <input type="text" id="cc_exp_month" autocomplete="off" data-stripe="exp-month" /> /
                            <input type="text" id="cc_exp_year" autocomplete="off" data-stripe="exp-year" /> 
                    </div>

                    <div class="field"><label for="cc_cvv"><strong>CVV</strong></label><br />
                            <!-- <input type="text" id="cc_cvv" autocomplete="off" value="123" /> -->
                            <input type="text" id="cc_cvv" autocomplete="off" data-stripe="cvc" />
                    </div>

                    <div class="field">
                            <label for="cc_first_name"><strong>First Name </strong></label><br />
                            <?php create_form_input('cc_first_name', 'text', $billing_errors, $values); ?>                            
                    </div>

                    <div class="field">
                            <label for="cc_last_name"><strong>Last Name </strong></label><br />
                            <?php create_form_input('cc_last_name', 'text', $billing_errors, $values); ?>                            
                    </div>

                    <div class="field">
                            <label for="cc_address"><strong>Street Address </strong></label><br />
                            <?php create_form_input('cc_address', 'text', $billing_errors, $values); ?>
                    </div>

                    <div class="field">
                            <label for="cc_city"><strong>City </strong></label><br />
                            <?php create_form_input('cc_city', 'text', $billing_errors, $values); ?>
                    </div>
                    

                    <div class="field">
                            <label for="cc_zip"><strong>Zip Code </strong></label><br />
                            <?php create_form_input('cc_zip', 'text', $billing_errors, $values); ?>
                    </div>


                    <div align="center" id="submit_div">
                        <input type="submit" value="Place Order" class="button" />
                    </div>

                </fieldset>
                
            </form>
            
        </div>        
    </div>    
</div>



<script type="text/javascript">    
    // Stripe.setPublishableKey('pk_test_73pRdYyHdGr83lEQLSe6FUjL'); //TEST KEY
    Stripe.setPublishableKey('pk_live_mPDJBg2L0blesjsNQr3434Ih'); //LIVE KEY
    
</script>

<script type="text/javascript">
    
    // This script validates the form and interacts with Stripe.
    $(function() {// Watch for the document to be ready:

        $('#billing_form').submit(function() {// capture the submit event, and then use the credit card information to create a single-use token:

                    var error = false;

                    // disable the submit button to prevent repeated clicks:
                    $('input[type=submit]', this).attr('disabled', 'disabled');

                    // Get the values:
                    //var cc_number = $('#cc_number').val(), cc_cvv = $('#cc_cvv').val(), cc_exp_month = $('#cc_exp_month').val(), cc_exp_year = $('#cc_exp_year').val();                    
                    var cc_number = $('#cc_number').val();            
                    var cc_cvv = $('#cc_cvv').val();
                    var cc_exp_month = $('#cc_exp_month').val();
                    var cc_exp_year = $('#cc_exp_year').val();
                    var name = $('#cc_first_name').val() + ' ' + $('#cc_last_name').val();
                    //var lName = $('#cc_last_name').val();
                    
                    
                    // Validate the number:
                    if (!Stripe.validateCardNumber(cc_number)) {
                        error = true;
                        reportError('The credit card number appears to be invalid.');
                    }

                    // Validate the CVC:

                    // Validate the expiration:
                    if (!Stripe.validateExpiry(cc_exp_month, cc_exp_year)) {
                        error = true;
                        reportError('The expiration date appears to be invalid.');
                    }
                    
                    if (!error) {
                        // Get the Stripe token:
                        //1st argument is the form element containing credit card data entered by the user. The relevant values are fetched from their associated inputs using the data-stripe attribute
                        //2nd argument stripeResponseHandler() is a callback that handles the response from Stripe. This handler function will either
                        //send the FORM to our SERVER if response was successful, OR if error occured this function displays that error.
                        Stripe.createToken({
                                number: cc_number,
                                cvc: cc_cvv,
                                exp_month: cc_exp_month,
                                exp_year: cc_exp_year,
                                name: name
                        }, stripeResponseHandler);
                    }

                    // prevent the form from submitting with the default action
                    return false;

        }); // form submission

    }); //Document Read


    // Function handles the Stripe response:
    function stripeResponseHandler(status, response) {

            // Check for an error:
            if (response.error) {
                
                /* 
                   // Show the errors on the form
                    $form.find('.payment-errors').text(response.error.message);
                    $form.find('button').prop('disabled', false);
                */
                
                reportError(response.error.message);
                    
            } else { // No errors, submit the form.
                                
              var billing_form = $('#billing_form');              
              var token = response.id;//response token contains id, last4, and card type
              
              // insert the token into the form so it gets submitted to the server
              billing_form.append("<input type='hidden' name='token' value='" + token + "' />"); //in order to add the token to the info submitted to your server, we're adding a new input tag into the form, and setting its value to the id of the token.              
              billing_form.get(0).submit();// and submit
              
            }

    } // End of stripeResponseHandler() function.



    function reportError(msg) {
        // Show the error in the form:
        $('#error_span').text(msg);
        
        // re-enable the submit button:
        $('input[type=submit]', this).attr('disabled', false);
            return false;
    }


</script>