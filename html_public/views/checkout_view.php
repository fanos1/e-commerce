
<!-- ========================
Display Carty 
======================== -->
<div class="container">    
    <div class="row">        
        <div class="col-lg-12">            

        <h2>Your Shopping Cart</h2>
        <table class="table table-bordered">

            <tr>
                <th align="center">image</th>
                <th align="center">Item</th>
                <th align="center">Quantity</th>
                <th align="right">Price</th>
                <th align="right">Subtotal</th>                    
            </tr>

            <?php

            $total = 0;

            // For removing problematic items:
            $remove = array();

            //Fetch each product
            foreach ($rows as $k => $array) {

                // Check the stock status:
                if ($array['stock'] < $array['quantity']) {

                    echo '
                    <tr class="text-danger">
                        <td colspan="4">
                            There are only ' . $array['stock'] . ' left in stock of the ' . $array['name'] . '. This item has been 
                                removed from your cart and placed in your wish list.
                        </td>
                    </tr>';

                    $remove[$array['sku']] = $array['quantity'];

                } else {

                    // Get the correct price:
                    $price = get_just_price($array['price'], $array['sale_price']);

                    // Calculate the subtotal:
                    $subtotal = $price * $array['quantity'];

                    // Print out a table row:
                    echo '
                    <tr>
                        <td>
                            <img src="/products/'.$array['image'].'" alt="'.$array['category'].'-'.$array['name'] .'" width="68" height="80" />
                        </td>
                        <td>' . $array['category'] . '-' . $array['name'] . '</td>
                        <td align="center">' . $array['quantity'] . '</td>
                        <td align="right">£' . $price . '</td>
                        <td align="right">£' . number_format($subtotal, 2) . '</td>
                    </tr>
                    ';

                    // Add the subtotal to the total:
                    $total += $subtotal;

                }
            }

            
            //$shipping = get_shipping($total);
            $shipping = 0; //FREE shipping
            $total += $shipping;
            echo '<tr>
                    <td colspan="2"> </td><th align="right">Shipping &amp; Handling</th>
                    <td align="right">£' . number_format($shipping, 2) . '</td>
            </tr>
            ';

            // Store the shipping in the session:
            $_SESSION['shipping'] = $shipping;

            // Display the total:
            echo '<tr>
                    <td colspan="2"> </td><th align="right">Total</th>
                    <td align="right">£' . number_format($total, 2) . '</td>
                    <td>&nbsp;</td>
            </tr>
            ';

            // Remove any problematic items:
            if (!empty($remove)) 
            {        
                // Clear the results:
                // ::: mysqli_next_result($dbc);



                // Loop through the array:
                foreach ($remove as $sku => $qty)  {

                    list($sp_type, $pid) = parse_sku($sku); //$remove Array stores [sku] as key

                    /* =================================================================
                    // Move it to the wish list:
                    $r = mysqli_multi_query(
                            $dbc, "CALL add_to_wish_list('$uid', '$sp_type', $pid, $qty);
                            CALL remove_from_cart('$uid', '$sp_type', $pid)"
                    );              
                    =============================================== 
                    */		


                    // Move it to the wish list:
                    $r = Cart::add_to_wish_list($dbc, $uid, $sp_type, $pid, $qty); //call
                    //if($r) { echo 'add to wish list successful'; }
                    

                    $r2 = Cart::remove_from_cart($dbc, $uid, $sp_type, $pid, $qty); //call                                
                    //if($r2) { echo 'remove from CART successful'; }

                }
            }

        ?>
        </table>

        </div>
    </div><!-- row -->
</div><!-- container -->
<!-- =============== END Display Carty ================ -->
<!-- =============== END Display Carty ================ -->





<div class="container">
    <div class="row">        
        <div class="col-lg-12">
            
            <h2>Your Shipping Information</h2>
            <p>
                Please enter your shipping information. On the next page,  you'll be able to enter your billing information 
                and complete the order. Please check the first box if your shipping and billing addresses are the same. 
                <span class="required">*</span> Indicates a required field. 
            </p>
            
            <form action="/checkout.php" method="POST" role="form">
                <?php include(INCLUDES. 'form_functions.inc.php'); ?>
                        
                        <div class="form-group">
                            <label for="use"><strong>Use Same Address for Billing?</strong></label><br />
                            <input type="checkbox" name="use" value="Y" id="use" <?php if (isset($_POST['use'])) echo 'checked="checked" ';?>/>
                        </div>    

                        <div class="from-group">
                            <label for="first_name"><strong>First Name <span class="required">*</span></strong></label><br />
                                <?php create_form_input('first_name', 'text', $shipping_errors); ?>
                        </div>

                        <div class="from-group">
                            <label for="last_name"><strong>Last Name <span class="required">*</span></strong></label><br />
                            <?php create_form_input('last_name', 'text', $shipping_errors); ?>
                        </div>

                        <div class="from-group">
                            <label for="address1"><strong>Street Address <span class="required">*</span></strong></label><br />
                            <?php create_form_input('address1', 'text', $shipping_errors); ?>
                        </div>

                        <div class="from-group">
                            <label for="address2"><strong>Street Address, Continued</strong></label><br />
                            <?php create_form_input('address2', 'text', $shipping_errors); ?>
                        </div>

                        <div class="from-group">
                            <label for="city"><strong>City <span class="required">*</span></strong></label><br />
                            <?php create_form_input('city', 'text', $shipping_errors); ?>
                        </div>

                        <!-- 
                        <div class="field">
                            <label for="state"><strong>State <span class="required">*</span></strong> </label><br />
                            <?php //create_form_input('state', 'select', $shipping_errors); ?>
                        </div> 
                        -->

                        <div class="from-group">
                            <label for="zip"><strong>Post Code <span class="required">*</span></strong></label><br />                            
                            <?php create_form_input('zip', 'text', $shipping_errors); ?>
                        </div>

                        <div class="from-group">
                            <label for="phone"><strong>Phone Number <span class="required">*</span></strong></label><br />
                            <?php create_form_input('phone', 'text', $shipping_errors); ?>
                        </div>

                        <div class="from-group">
                            <label for="email"><strong>Email Address <span class="required">*</span></strong></label><br />
                            <?php create_form_input('email', 'text', $shipping_errors); ?>
                        </div>

                        <br clear="all" />

                        <div align="center">
                            <input type="submit" value="Continue to Billing" class="btn btn-success" />
                        </div>

            </form>
        </div>        
    </div><!-- row -->
</div><!-- container -->








