


<div class="container" style="padding:2em;">
    <div class="row">

        <div class="col-lg-9">
            <!-- 1st row -->
            <div class="row">
                <div class="col-lg-4 animated bounceInLeft">
                    <a href="/sales.php" title="to sales">
                        <img id="img1" src="images/non-purplish1.jpg" alt="sale items" />
                    </a>                    
                </div>
                <div class="col-lg-4">
                    <img id="img2" class="pull-right animated rollIn" src="images/look-sexy-400.png" alt="sexy dresses" />
                </div>
                <div class="col-lg-4 animated bounceInRight" id="best-seller">
                    <a href="/category/cardigans/3" title="to tunics">
                        <img id="img3" class="img-responsive" src="images/cool-girl.jpg" alt="best cardigan sellers" />
                    </a>
                </div>
            </div>

            <hr />

            <!-- 2nd row -->
            <div class="row">
                <div class="col-lg-6 animated bounceInUp" id="jumper-container">
                    <a href="/category/jumpers/1" title="to jumpers page">
                        <img id="img4" class="img-responsive" src="images/jumpers.jpg" alt="jumpers" />
                    </a>
                </div>
                <div class="col-lg-6 animated bounceInRight" id="tunic-container">
                    <a href="/category/jumpers/2" title="to tunics">
                        <img id="img5" class="img-responsive" src="images/purplish-380.jpg" alt="tunics" />
                    </a>
                </div>
            </div>

        </div><!-- col-lg-9 -->


        <div class="col-lg-3" id="hide-video" style="overflow:hidden;  padding:0;">            
            <video loop="" preload="auto" width="300" autoplay="" style=" margin: -1%; width: 102%;">
                <source src="images/movie2.mp4" type="video/mp4"> <!-- for Firefox -->
                <source src="images/movie2.mov">
                Your browser does not support HTML5 video.  
            </video> 
            <h1 style="padding:0.25em 2em 0 0; 
               color:#999;
               font-size: 16px;
               text-align: center;
               text-transform: uppercase;">
                Look great with our latest collection clothes. 
                Not only do our clothes make you look beautiful, but they are also made from the best material.
            </h1>               
        </div> 

    </div>
</div><!-- CONTAINER -->




<!-- NEWSLETTER -->
<style type="text/css">
    .response-waiting {
        background-image: url("loading.gif") no-repeat;
        color: red;
    }
    .response-success {
        color: red;
        background-image: url(tick.gif) no-repeat;
     }

     .response-error {
         color: red;
        background-image: url("cross.gif") no-repeat;
     }
</style>


<script language="javascript" type="text/javascript">
		
            
            $( document ).ready(function() {
                
                // Variable to hold request
                var request;

                // Attach the submit event of our form
                $("#foo").submit(function(event) {

                    // Abort any pending request
                    if (request) {
                        request.abort();
                    }
                    
                    // setup some local variables
                    var $form = $(this);

                    // Let's select and cache all the fields
                    var $inputs = $form.find("input, select, button, textarea");

                    // Serialize the data in the form
                    var serializedData = $form.serialize();

                    // Let's disable the inputs for the duration of the Ajax request.
                    // Note: we disable elements AFTER the form data has been serialized.
                    // Disabled form elements will not be serialized.
                    $inputs.prop("disabled", true);
                    
                     /* 
                      As we are going to be using AJAX to submit the form, we should provide the end user an indication 
                      that we are doing some work behind the scenes, so to speak. To do so, we will set the response 
                      message holders text to 'Please Wait...' and give it a class of 'response-waiting'. This will give it a cool loading gif 
                     */
                     $("#displayHere").hide()  //We hide the response message holder first so that when we set the text, it does not show straight away, we will fade in
                        .addClass('response-waiting')
                        .text('Please Wait...')
                        .fadeIn(200);

                    // Fire off the request to /form.php
                    request = $.ajax({
                        url: "/ajax-news-process.php",
                        type: "post",
                        data: serializedData
                    });

                    // Callback handler that will be called on success
                    request.done(function (response, textStatus, jqXHR) {
                                                
                        //if you want to get any returned data from the PHP file, response OBJ has it
                        //alert(response);
                        
                        
                        //IF WE USE JSON IN THE PHP FILE, THE response is  JSON encoded, and we need to parse this json so that we can use it
                        //$("#displayHere").html(response);  OUT::: {"status":"status ok","message":"messa here"}
                        
                        //var obj = jQuery.parseJSON( '{ "name": "John" }' ); 
                        //alert( obj.name === "John" );
                        
                        var parsedRespon = jQuery.parseJSON( response );                        
                        var klass = '';
                        
                        if(parsedRespon.status == 'success') {
                            //$("#displayHere").html('fdsfdsf');
                            klass = "response-success";
                        } else if (parsedRespon.status == 'error') {
                            //$("#displayHere").html('ERRRR ::: ');
                            //$("#displayHere").html(parsedRespon.message);
                            klass = "response-error";
                        } 
                        
                            //show reponse message to user
                            $("#displayHere").fadeOut(2000,function() {
                               $(this).removeClass('response-waiting')
                                      .addClass(klass)
                                      //.text(parsedRespon.message)
                                      .html(parsedRespon.message)
                                      .fadeIn(200,function(){
                                            //set timeout to hide response message
                                            setTimeout(function() {
                                                    $("#displayHere").fadeOut(200,function(){
                                                            $(this).removeClass(klass);
                                                            //form.data('formstatus','idle');
                                                    });
                                             },3000)
                                     });
                            });
                        
                    });

                    // Callback handler that will be called on failure
                    request.fail(function (jqXHR, textStatus, errorThrown){
                        // Log the error to the console
                        //console.error("The following error occurred: "+textStatus, errorThrown);
                        //$("#displayHere").html('An Error Occured, we apologize!');
                        alert('An error occured, We apologize! ');
                    });

                    // Callback handler that will be called regardless if the request failed or succeeded
                    request.always(function () {
                        // Reenable the inputs
                        $inputs.prop("disabled", false);
                    });

                    // Prevent default posting of form
                    event.preventDefault();
                });

                
            });
            
</script>


<div class="container" style="padding:2em;">	
    <div class="subscribe">
        <div class="row">

            <div class="col-sm-12 col-md-3 col-lg-4">
                <h3 id="newsletter"> NEWSLETTER SIGNUP</h3>
            </div>

            <div class="col-sm-12 col-md-9 col-lg-8">
                <form class="form-inline" id="foo">
                    <div class="row">
                        <div class="col-sm-5 col-md-5">
                            <input type="text" class="form-control pull-right" size="32" name="bar" value="Your E-mail..." />								
                        </div>							
                        <div class="col-sm-2 col-md-2">
                            <button type="submit" class="button btn-cool" data-toggle="modal" data-target="#myModal">
                                <span class="glyphicon glyphicon-envelope" style="color:#FF1493;"> </span>SUBSCRIBE
                            </button>
                        </div>							
                        <div class="col-sm-5 col-md-4">
                            <p>Enter your email to get promotions</p>
                        </div>
                    </div>   
                </form>
            </div>
            
            
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thank you for signing up with us!</h4>
                  </div>
                  <div class="modal-body">
                    <p id="displayHere">  </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- END MODAL -->

        </div>
    </div>    
</div>  



