


<div class="container"> 
    <div class="row">
        <?php
        foreach ($rows as $k => $array) {	          
            /* 
            echo '
            <h3 id="' . $array['sku'] . '">' . $array['category'] . '::' . $array['name'] .'</h3>
            <div class="img-box">
                <p>
                    <img alt="' . $array['name'] . '" src="/products/' . $array['image']  . '" />' . 
                    $array['description'] . '<br />' . 
                    get_price('goodies', $array['price'], $array['sale_price']) . '
                    <strong>Availability:</strong> ' . get_stock_status($array['stock']) . '
                </p>
                <p><a href="/cart.php?sku=' . $array['sku'] . '&action=add" class="button">Add to Cart</a></p>
            </div>';
             * 
             */
            echo '
            <div class="col-md-4" itemtype="http://schema.org/Product">                        
                <div class="thumbnail" style="padding:0; ">

                    <h3 style="text-align:center;" id="' . $array['sku'] . '">' . $array['category'] . '::' . $array['name'] .'</h3>

                    <a href="/item_details.php?sku=' . $array['sku'] . '" title="' . $array['name'] . '">
                        <img alt="' . $array['name'] . '" src="/products/' . $array['image']  . '" width="280" />
                    </a>
                    <div class="caption">
                        <p style="text-align:center;"><strong>'.$array['name'].'</strong></p>
                        <div style="text-align:center;">'.get_price('goodies', $array['price'], $array['sale_price']).'</div>  
                    </div>

                </div>                        
            </div>';

        } 
        ?>
    </div>
</div><!-- container -->

 