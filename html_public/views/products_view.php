<?php

if (is_array($rows) || is_object($rows)) { //some queries are UPDATE and INSERT. This queries will return Empty 0 rows, which means $rows will NOT be Array. We want to loop only if $rows > 0. 
    
    foreach ($rows as $k => $array) {	          
        echo '<h3>' . $array['name'] . '</h3>
            <div class="img-box">
                <p>
                    <img alt="' . $array['name'] . '" src="/products/' . $array['image']  . '" />' . 
                    $array['description'] . '<br />' . get_price($type, $array['price'], $array['sale_price']) . 
                    '<strong>Availability:</strong> ' . get_stock_status($array['stock']) . 
                '</p>
                 
                <p><a href="/cart.php?sku=' . $array['sku'] . '&action=add" class="button">Add to Cart</a></p>
                <p><a href="/item_details.php?sku=' . $array['sku'] . '" class="button">Details</a></p>
                    
            </div>';
    }              

}        
    
    