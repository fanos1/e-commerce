<?php
$page_title = 'Dobaln Ladies clothing | Contact Us';
include './includes/header.php';
?>

<style>
    #map {
      width: 100%;
      height: 330px;
    }
</style>

    
<script src="https://maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript">
   
    function initialize() {
        
        //mapTypeId is used to specify what type of map to use. Your choices are ROADMAP, SATELLITE, HYBRID, or TERRAIN.
        var map_canvas = document.getElementById('map');
        var map_options = {
               center : new google.maps.LatLng(51.605334, -0.072827),  //center is a Google Maps LatLng object that tells the the API where to center the map.
               zoom : 15,                                              //zoom is a number between 0 (farthest) and 22 that sets the zoom level of the map.
               mapTypeId : google.maps.MapTypeId.ROADMAP               //mapTypeId is used to specify what type of map to use. Your choices are ROADMAP, SATELLITE, HYBRID, or TERRAIN. 
        };
        var mapPin = "http://www.google.com/mapfiles/marker.png"; //get icon
        var Marker = new google.maps.Marker({
               position : map_options.center,  
        });
        var map = new google.maps.Map(map_canvas, map_options)
        Marker.setMap(map);
    } 
    google.maps.event.addDomListener(window, 'load', initialize); //Call
    

</script>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="map">                  
            </div>
        </div>
    </div>
</div>




<div class="container">    
    <div class="row">

        <section id="sidebar-main" class="col-md-12">
                        
                <div class="col-ld-4 col-md-4 col-sm-12 hidden-xs">
                    <div class="contact-info">
                        <h3>Contact Us</h3>
                        <div>
                            <div class="media">
                                <i class="fa fa-home pull-left"></i>
                                <div class="media-body">
                                    DOBALN FASHION LTD.
                                    <br> 74a White Hart Lane
                                    <br />London
                                    <br />N17 8HP
                                    <br />United Kingdom
                                </div>
                            </div>
                        </div>
                    </div>                                
                </div>

                <div class="col-ld-8 col-md-8 col-sm-12">
                    <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal ">
                        <fieldset class="">
                            <h3>Contact Form</h3>
                            <div class="content">
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-name">Your Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" value="" id="input-name" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-email">E-Mail Address</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" value="" id="input-email" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-enquiry">Enquiry</label>
                                    <div class="col-sm-10">
                                        <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <div class="pull-right">
                                        <input class="btn btn-primary" type="submit" value="Submit" />
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

        </section>
        
    </div><!-- row -->
</div><!-- container -->