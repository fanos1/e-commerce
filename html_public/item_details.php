<?php
require('./includes/config.inc.php');
include('./includes/product_functions.inc.php');// Need the utility functions:

// If there's a SKU value in the URL, break it down into its parts:
if (isset($_GET['sku'])) {
    
    list($type, $pid) = parse_sku($_GET['sku']);    
}



if (isset($type, $pid) && filter_var($pid, FILTER_VALIDATE_INT, array('min_range' => 1)) ) {
	
    $pid = $pid;    
    //echo "<h3>$itemId</h3>";
        
    /* 
    if ($_GET['type'] === 'goodies') {		
        $type = 'goodies';		
    } elseif ($_GET['type'] === 'coffee') {		
        $type = 'coffee';			
    } 
     * 
     */
    
} else {
    $page_title = 'Error!';        
    include(INCLUDES. 'header.php');
    include ( VIEWS . "error_view.php" ); 
    include(INCLUDES. 'footer.php');
    exit();
}



require(PDO);
try {
    $dbc = dbConn::getConnection();
} catch (Exception $ex) {    
    exit("<h3>An Error Occured, We apologise</h3>");
}

include(MODELS. 'Product.php');
$rows = Products::select_product_byID($dbc, $pid);


//============= HTML ==============
// ============ HTML ==============
include(INCLUDES. 'header.php');
if ($rows) {       
    
    $page_title = $rows[0]['name'];
    
    if ($type === 'goodies') {                        

        //1 record (row) expected
        
        include ( VIEWS . "itemdetails_view.php" ); 
        
    } elseif ($type === 'coffee') {
                   
        //include ( VIEWS . "coffees_view.php" ); 
        exit('cik, items_details.php file');        
        /* 
        // Clear the stored procedure results:
        mysqli_next_result($dbc);    
                  
        // Handle and show reviews...  
        include('./includes/handle_review.php');

        $r = mysqli_query($dbc, "CALL select_reviews('$type', $sp_cat)");
        include('./views/review.html'); 
         * 
         */
        
        //mysqli_next_result($link) :: Prepare next result from multi_query

    }
    
} else {
    //echo 'NO ROWS';
    include ( VIEWS . "noproducts_view.php" ); 
}

// Include the footer file:
include('./includes/footer.php');
?>